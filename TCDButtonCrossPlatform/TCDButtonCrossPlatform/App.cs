﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Xamarin.Forms;
using TCDButtonCrossPlatform.Views;
using System.Diagnostics;
using System.Threading.Tasks;


namespace TCDButtonCrossPlatform
{
	public class App : Application
    {
		
		public App()
		{
			
			MainPage = new NavigationPage(new StartupPage()){BarBackgroundColor=Color.FromHex("333333"), BarTextColor = Color.White};

		}

		protected override async void OnStart()
		{
			string url = "http://"+TCDButtonCrossPlatform.Helpers.Settings.Server;
			//string url = "http://mule.tcd.com.au";
			JsonStuff ws = new JsonStuff (url);
			string test = await ws.GetGeolocation ();
			Debug.WriteLine ("getGeoString bray: ");
			Debug.WriteLine (test);

		}


    }
}
