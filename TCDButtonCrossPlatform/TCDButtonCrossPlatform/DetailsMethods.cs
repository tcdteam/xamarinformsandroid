using System;
using System.Collections.Generic;
//using System.Web.Script.Serialization;
using Newtonsoft.Json;
//using Xamarin.Payments.Stripe;


namespace TCDSupportButton
{
    public class DetailsMethods
    {
        jasonStuff js;
        private string _ConnectionString;        

        public DetailsMethods(string pConnectionString)
        {
            js = new jasonStuff(pConnectionString);
            _ConnectionString = pConnectionString;
        }
			
		public string GetGeoLocation ()
		{
			try {
				string rPonce = js.GetGeolocation();
				if (rPonce.ToLower().Contains("success")) {
					return "success";
				}
				else
				{
					return "fail";
				}
			}
			catch
			{
				return "fail to connect";
			}
		}
			

		public List<jasonStuff.TicketHistory> GetTicketHistory(string email)
		{
			try
			{
				return js.GetTicketHistory(email);
			}
			catch
			{
				return null;
			}
		}

		public List<jasonStuff.Contact> GetDynamicsContact(string email, string timeoffday, string gpslong, string gpslat)
		{
			try
			{
				return js.GetDynamicsContact(email, timeoffday, gpslong, gpslat);
			}
			catch
			{
				return null;
			}
		}
			
		public List<jasonStuff.Message> GetDynamicsMessage(string email, string timeoffday, string gpslong, string gpslat)
		{
			try
			{
				return js.GetDynamicsMessage(email, timeoffday, gpslong, gpslat);
			}
			catch
			{
				return null;
			}
		}

		public bool InsertTicket (string device_id, string device_type, string ticket_id, string ticket_subject, string ticket_detail, string company, string ticket_email, string ticket_phone, string lat, string lon, string time)
		{
			try {
				string rPonce = js.InsertTicket(device_id,device_type,ticket_id,ticket_subject,ticket_detail,company,ticket_email, ticket_phone, lat,lon,time );
				if (rPonce.ToLower().Contains("success")) {
					return true;
				}
				return false;
			} catch {
				return false;
			}
		}
			
		public bool InsertUser_Details (string user_name, string user_device_id, string user_company, string user_email, string user_phone)
		{
			try {
				string rPonce = js.InsertUser_Details(user_name,user_device_id,user_company,user_email,user_phone);
				if (rPonce.ToLower().Contains("success")) {
					return true;
				}
				return false;
			} catch {
				return false;
			}
		}

		/*
		public StripeCustomer GetCustomerByEmail(string email, string user_device_id){
			StripeCustomer sc = js.GetCustomerByEmail (email, user_device_id);
			return sc;
		}

		public StripeCharge Charge(int amount, string currency, string customer, string description){
			StripeCharge sc = js.Charge(amount, currency, customer, description);
			return sc;
		}

		public StripeCharge ChargeWithToken(int amount, string currency, StripeCreditCardInfo cc, string description){
			StripeCharge sc = js.ChargeWithToken(amount, currency,cc,description);
			return sc;
		}

		public bool InsertUser_DetailsCC (string email, string user_device_id, string customer_id)
		{
			try {
				string rPonce = js.InsertUser_DetailsCC(email,user_device_id,customer_id);
				if (rPonce.ToLower().Contains("success")) {
					return true;
				}
				return false;
			} catch {
				return false;
			}
		}

		public StripeCustomer UpdateBalanceCustomer(string customer_id, long balance)
		{
			StripeCustomerInfo sci = new StripeCustomerInfo ();
			sci.AccountBalance = balance;
			return js.UpdateBalanceCustomer (customer_id,sci);

		}
		public StripeCard UpdateCard(string customer_id, StripeCreditCardInfo scci){
			return js.UpdateCard (customer_id, scci);
		}

		public bool Delete_Customer (string email, string user_device_id)
		{
			try {
				string rPonce = js.Delete_Customer(email,user_device_id);
				if (rPonce.ToLower().Contains("success")) {
					return true;
				}
				return false;
			} catch {
				return false;
			}
		}

		public StripeCustomer DeleteCustomerStripe(string customer_id){
			return js.DeleteCustomerStripe (customer_id);
		}
		*/
	}

}

