﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace TCDButtonCrossPlatform
{
    public class MenuListData : List<MenuItem>
    {
        public MenuListData()
        {
            this.Add(new MenuItem()
            {
                Title = "Issue",
                IconSource = "issue.png",				
				TargetType = typeof(FormPage)
            });

//			this.Add(new MenuItem()
//			{
//				Title = "Attach",
//				IconSource = "attach.png",
//				 TargetType = typeof(LeadsPage)
//			});
//
//			this.Add(new MenuItem()
//			{
//				Title = "Location",
//				IconSource = "map.png",
//				 TargetType = typeof(LeadsPage)
//			});

            this.Add(new MenuItem()
            {
                Title = "Call & SMS",
                IconSource = "sms.png",
				TargetType = typeof(ContactPage)
            });
            this.Add(new MenuItem()
            {
                Title = "Ticket History",
                IconSource = "history.png",
				TargetType = typeof(TicketHistoryPage)
            });

            this.Add(new MenuItem()
            {
                Title = "Configuration",
                IconSource = "connection.png",
				TargetType = typeof(ConnectionPage)
            });
			this.Add(new MenuItem()
			{
				Title = "Close",
				IconSource = "close.png",
				  TargetType = typeof(StartupPage)
			});
       
        }
    }
}
