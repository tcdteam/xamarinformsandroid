﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;
using System.Linq;
//using Xamarin.Payments.Stripe;
using System.ComponentModel;
using System.Threading.Tasks;

public class jasonStuff
{

	public string URLConnectionString;

	private Dictionary<string, object> tokenizedResponse;

	public Dictionary<string, object> getResponseAsAToken ()
	{
		return tokenizedResponse;
	}

	public jasonStuff (string pURLConnectionString)
	{
		URLConnectionString = pURLConnectionString;
		tokenizedResponse = new Dictionary<string, object> ();
	}
		

	public string getValue (string key)
	{
		if (tokenizedResponse != null) {
			if (tokenizedResponse ["resourse"].ToString ().Contains (key)) {
				Dictionary<string,string> temp = new Dictionary<string,string> ();
				JObject obj = JObject.Parse (tokenizedResponse ["resourse"].ToString ());
				foreach (var kvp in obj) {
					temp.Add (kvp.Key, kvp.Value.ToString ());
				}

				return temp [key];
			}

		}        
		return null;
	}
		
	//Done
	private string getWebRequest (string pURL)
	{
		try {		   
			string rawresp = "";
//			HttpWebRequest request = default(HttpWebRequest);
//			HttpWebResponse responses = null;
//			StreamReader reader = default(StreamReader);

//			request = (HttpWebRequest)WebRequest.Create (pURL);
//			responses = (HttpWebResponse)request.GetResponse();
//			reader = new StreamReader (responses.GetResponseStream ());
//			rawresp = reader.ReadToEnd ();

			//Console.WriteLine (pURL);
			//Console.WriteLine (rawresp);




			return rawresp;
		} catch  {

			//Console.WriteLine (ex.ToString ());
			return "ERROR";

		}
	}


	public void JSONresponseAsToken (string response)
	{
		JObject obj = JObject.Parse (response);
		foreach (var kvp in obj) {
			tokenizedResponse.Add (kvp.Key, kvp.Value);
		}
	}
		

	public string InsertTicket (string device_id, string device_type, string ticket_id, string ticket_subject, string ticket_detail, string company, string ticket_email, string ticket_phone, string lat, string lon, string time)
	{
	
		string url;
		url = URLConnectionString + Convert.ToString ("/ticket?api_method=insertticket&device_id=") + device_id + Convert.ToString ("&device_type=") + device_type + Convert.ToString ("&ticket_id=") + ticket_id + Convert.ToString ("&ticket_subject=") + ticket_subject + Convert.ToString ("&ticket_detail=") + ticket_detail + Convert.ToString ("&company=") + company + Convert.ToString ("&ticket_email=") + ticket_email + Convert.ToString ("&ticket_phone=") + ticket_phone + Convert.ToString ("&lat=") + lat + Convert.ToString ("&lon=") + lon + Convert.ToString ("&time=") + time;

		return getWebRequest (url);
	}

	public string InsertUser_Details (string user_name, string user_device_id, string user_company, string user_email, string user_phone)
	{

		string url;
		url = URLConnectionString + Convert.ToString ("/ticket?api_method=insertuser_details&user_name=") + user_name + Convert.ToString ("&user_device_id=") + user_device_id + Convert.ToString ("&user_company=") + user_company + Convert.ToString ("&user_email=") + user_email + Convert.ToString ("&user_phone=") + user_phone ;

		return getWebRequest (url);
	}

	public string GetGeolocation ()
	{
	
		string url;
		url = URLConnectionString + Convert.ToString ("/ticket?api_method=getgeolocation");

		return getWebRequest (url);
	}

	public class Contact
	{
		public string ContactSMS { get; set; }
		public string ContactPhone { get; set; }
	}

	public List<Contact > GetDynamicsContact (string email, string timeoffday, string gpslong, string gpslat)
	{
		try
		{
			string rawresp = "";
			Dictionary<string,object> rd = new Dictionary<string,object> ();

			string url;
			url = URLConnectionString + Convert.ToString ("/ticket?api_method=getphone_number&submitteremail=")+email+
			Convert.ToString("&timeoffday=")+timeoffday+
			Convert.ToString("&gpslong=")+gpslong+
			Convert.ToString("&gpslat=")+gpslat;
			rawresp = getWebRequest (url);

			List<Contact > tList = new List<Contact >();
			JObject obj1 = JObject.Parse (rawresp);
			foreach (var contact in obj1["resourse"]){
					tList.Add( JsonConvert.DeserializeObject<Contact>(contact.ToString()));
			}
			return tList;
		}
		catch 
		{
			return null;
		}
	}

	public class Message
	{
		public string ContactMessage { get; set; }
	}

	public List<Message > GetDynamicsMessage (string email, string timeoffday, string gpslong, string gpslat)
	{
		try
		{
			string rawresp = "";
			Dictionary<string,object> rd = new Dictionary<string,object> ();

			string url;
			url = URLConnectionString + Convert.ToString ("/ticket?api_method=getmessage&submitteremail=")+email+
				Convert.ToString("&timeoffday=")+timeoffday+
				Convert.ToString("&gpslong=")+gpslong+
				Convert.ToString("&gpslat=")+gpslat;
			rawresp = getWebRequest (url);

			List<Message > tList = new List<Message >();
			JObject obj1 = JObject.Parse (rawresp);
			foreach (var message in obj1["resourse"]){
				tList.Add( JsonConvert.DeserializeObject<Message>(message.ToString()));
			}
			return tList;
		}
		catch 
		{
			return null;
		}
	}

	public class TicketHistory
	{
		public long Id { get; set; }
		public string Ticket_subject { get; set; }
		public string Ticket_detail { get; set; }
	}

	public List<TicketHistory > GetTicketHistory (string email)
	{
		try
		{
			string rawresp = "";
			Dictionary<string,object> rd = new Dictionary<string,object> ();

			string url;
			url = URLConnectionString + Convert.ToString ("/ticket?api_method=gethistory&submitteremail=") + email;
			rawresp = getWebRequest (url);
			
			List<TicketHistory > tList = new List<TicketHistory >();
				JObject obj1 = JObject.Parse (rawresp);
				foreach (var tickethistory in obj1["resourse"]){
					tList.Add( JsonConvert.DeserializeObject<TicketHistory>(tickethistory.ToString()));
				}
			return tList;
		}
		catch 
		{
			return null;
		}
	}
	public class Customer{
		public string user_company{ get; set;}
		public string user_email{ get; set;}
		public string user_name{ get; set;}
		public string user_phone{ get; set;}
		public string user_login{ get; set;}
		public string user_password{ get; set;}
		public string customer_id{ get; set;}
		public string user_device_id{ get; set;}
		public string id{ get; set;}
	}

	public List<Customer> GetCustomer(string email, string user_device_id)
	{
		try
		{	

			string rawresp = "";
			Dictionary<string,object> rd = new Dictionary<string,object> ();

			string url;
			url = URLConnectionString + Convert.ToString ("/ticket?api_method=getcustomer_id&user_email=") + email + Convert.ToString ("&user_device_id=") + user_device_id ;
			rawresp = getWebRequest (url);

			List<Customer> tList = new List<Customer>();
			JObject obj1 = JObject.Parse (rawresp);


			foreach (var customer in obj1["resourse"]){
				tList.Add( JsonConvert.DeserializeObject<Customer>(customer.ToString()));
			}

			return tList;
		}
		catch 
		{
			return null;
		}
	}

	/*
	 * function stripe
	 * 
	public StripeCustomer GetCustomerByEmail(string email, string user_device_id){
		StripePayment payment = new StripePayment ("sk_test_yeND8RTKSzEtE0ebJJQHM0Ic");
		List<Customer> customer_str = GetCustomer (email, user_device_id);

		try{
			if(customer_str[0].customer_id==null){
				return null;
			}
			StripeCustomer objectCustomer = payment.GetCustomer (customer_str [0].customer_id);
			Console.WriteLine("customer_str[0].customer_id: "+customer_str[0].customer_id);
			return objectCustomer;
		}catch {
			return null;
		}

	}

	public StripeCharge Charge(int amount, string currency, string customer, string description){
		StripePayment payment = new StripePayment ("sk_test_yeND8RTKSzEtE0ebJJQHM0Ic");
		return payment.Charge(amount,currency,customer,description);
	}

	public StripeCharge ChargeWithToken(int amount, string currency, StripeCreditCardInfo cc, string description){
		StripePayment payment = new StripePayment ("sk_test_yeND8RTKSzEtE0ebJJQHM0Ic");
		StripeCreditCardToken tok = payment.CreateToken (cc);
		Console.WriteLine ("Token Pay"+tok.ID);
		return payment.ChargeWithToken (amount,currency,tok.ID,description);
	}

	public string InsertUser_DetailsCC (string email, string user_device_id,string customer_id)
	{

		string url;
		url = URLConnectionString + Convert.ToString ("/ticket?api_method=insertuser_detailscc&user_email=") + email +
									Convert.ToString ("&user_device_id=") + user_device_id + 
									Convert.ToString ("&customer_id=") + customer_id;
		return getWebRequest (url);
	}

	public StripeCustomer UpdateBalanceCustomer(string customer_id,StripeCustomerInfo sci)
	{
		StripePayment payment = new StripePayment ("sk_test_yeND8RTKSzEtE0ebJJQHM0Ic");
		Console.WriteLine ("account balance update :"+sci.AccountBalance);
		return payment.UpdateCustomer (customer_id,sci);
	}

	public StripeCard UpdateCard(string customer_id, StripeCreditCardInfo succi){
		StripePayment payment = new StripePayment ("sk_test_yeND8RTKSzEtE0ebJJQHM0Ic");
		StripeCustomer sc = payment.GetCustomer (customer_id);
		payment.DeleteCard (customer_id,sc.Cards.Data[0].ID);

		return payment.CreateCard (customer_id, succi);
	} 

	public string Delete_Customer (string email, string user_device_id)
	{
		string url;
		url = URLConnectionString + Convert.ToString ("/ticket?api_method=deletecustomer_id&user_email=") + email +
		Convert.ToString ("&user_device_id=") + user_device_id;
		return getWebRequest (url);
	}

	public StripeCustomer DeleteCustomerStripe(string customer_id){
		StripePayment payment = new StripePayment ("sk_test_yeND8RTKSzEtE0ebJJQHM0Ic");
		return payment.DeleteCustomer (customer_id);
	}
	*/
}
