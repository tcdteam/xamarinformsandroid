﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;
using Xamarin.Forms.Maps;





using XLabs.Ioc;
using XLabs.Platform.Device;
using XLabs.Platform.Services.Media;
using System.Threading.Tasks;
using XLabs.Platform.Services.Geolocation;
using System.Threading;
using System.Diagnostics;
using System.Net;
using System.IO;
using System.Text;
using System.Net.Http;

using System.Reflection;

namespace TCDButtonCrossPlatform
{
	public class FormPage : ContentPage
	{



		Image pic;
		Editor issue;
		Map map;
		Label labelTitle;
		Label labelFile;
		JsonStuff ws;
		string longlat;
		List<string> imagesList = new List<string>();
		List<string> fileList = new List<string>();

		List<string> allFile = new List<string>();

		string tempChooseDialog = "";

		byte[] tempImage;

		Entry name;
		Entry email;
		Image tcdlogo;
		Entry company;
		Entry subject;
		Entry phone;


		Button submit;
		Switch isremember;
		Image btnattach;
		Image btnlocation;
		Image btnissue;
		Image btnRecord;
		Image btnStopRecord;

		ActivityIndicator indicator;


		public double longitude { get; set; }
		public double latitude { get; set; }
		/// <summary>
		/// The device
		/// </summary>
		private IDevice _device;
		/// <summary>
		/// The media picker
		/// </summary>
		private IMediaPicker _mediaPicker;
		/// <summary>
		/// The scheduler
		/// </summary>
		private readonly TaskScheduler _scheduler = TaskScheduler.FromCurrentSynchronizationContext();
		/// <summary>
		/// The path
		/// </summary>
		private string _path;
		/// <summary>
		/// The geolocator
		/// </summary>
		private IGeolocator _geolocator;
		/// <summary>
		/// The cancel source
		/// </summary>
		private CancellationTokenSource _cancelSource;
		private CancellationTokenSource cts;

		private ImageSource _imageSource;
		/// <summary>
		/// Gets or sets the position status.
		/// </summary>
		/// <value>The position status.</value>
		public string PositionStatus
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the position latitude.
		/// </summary>
		/// <value>The position latitude.</value>
		public string PositionLatitude
		{
			get;
			set;

		}

		/// <summary>
		/// Gets or sets the position longitude.
		/// </summary>
		/// <value>The position longitude.</value>
		public string PositionLongitude
		{
			get;
			set;
		}
		/// <summary>
		/// Gets or sets the status.
		/// </summary>
		/// <value>The status.</value>
		public string Status
		{
			get;
			set;
		}

		public FormPage()
		{
			BackgroundColor = Device.OnPlatform(Color.FromHex("333333"), Color.Default, Color.Default);
			Title = "TCD Support Form";
			//getPhoneNumber ();
			ws = new JsonStuff("http://" + TCDButtonCrossPlatform.Helpers.Settings.Server);
			//Icon = "icon.png";
			pic = new Image { Aspect = Aspect.AspectFit, HeightRequest = 130, VerticalOptions = LayoutOptions.CenterAndExpand, IsVisible = !string.IsNullOrEmpty(_path) };
			issue = new Editor { HeightRequest = 130, IsVisible = true };
			map = new Map(new MapSpan(new Xamarin.Forms.Maps.Position(latitude, longitude), 360, 360))
			{
				//map = new Map () {
				VerticalOptions = LayoutOptions.FillAndExpand,
				HeightRequest = 130,
				BackgroundColor = Color.Blue,
				IsVisible = false
			};
			labelTitle = new Label { Text = "File Attached :", IsVisible = false };
			labelFile = new Label { Text = "File Attached :", IsVisible = false };



			name = new Entry { Placeholder = "Name" };
			email = new Entry { Placeholder = "Email" };
			tcdlogo = new Image { Source = "tcdlogo.png", Aspect = Aspect.AspectFit, HorizontalOptions = LayoutOptions.Fill };
			company = new Entry { Placeholder = "Company" };
			subject = new Entry { Placeholder = "Subject" };
			phone = new Entry { Placeholder = "Phone Number" };


			submit = new Button { BackgroundColor = Color.Blue, BorderRadius = 0, Text = "SUBMIT" };
			isremember = new Switch { HorizontalOptions = LayoutOptions.Start };
			btnattach = new Image { Source = "btnattach.png", HorizontalOptions = LayoutOptions.Start };
			btnlocation = new Image { Source = "btnmap.png", HorizontalOptions = LayoutOptions.Center };
			btnissue = new Image { Source = "btnissue.png", HorizontalOptions = LayoutOptions.Center };
			btnRecord = new Image { Source = "btnrecord.png", HorizontalOptions = LayoutOptions.End };
			btnStopRecord = new Image { Source = "btnstoprecord.png", HorizontalOptions = LayoutOptions.End, IsVisible = false };
			indicator = new ActivityIndicator { HorizontalOptions = LayoutOptions.CenterAndExpand, Color = Color.White };
			indicator.IsRunning = false;

			SetupCamera();

			var tap = new TapGestureRecognizer();
			tap.Tapped += async (sender, e) =>
			{



				var action = await DisplayActionSheet("Choose upload type", "Cancel", null, "File", "Image");
				tempChooseDialog = action;

				if (action == "File")
				{
					getFile();

					//labelFile.Text = textFile;
					//					labelFile.IsVisible = true;
					//					labelTitle.IsVisible = true;
					pic.IsVisible = false;

				}
				else if (action == "Image")
				{
					await TakePicture();
					//pic.IsVisible = true;

					//if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
					//{
					//	DisplayAlert("No Camera", ":( No camera avaialble.", "OK");
					//	return;
					//}

					//string ahstr = DateTime.Now.ToString("yy-MM-dd");

					//var file = await CrossMedia.Current.TakePhotoAsync(new Plugin.Media.Abstractions.StoreCameraMediaOptions
					//{

					//	Directory = "pict",
					//	Name = "pic_"+ahstr+".jpg"

					//});

					//if (file == null)
					//	return;

					//DisplayAlert("File Location", file.Path, "OK");

					//pic.Source = ImageSource.FromStream(() =>
					//{
					//	var stream = file.GetStream();
					//	_path = file.Path;
					//	//file.Dispose();

					//	using (var memoryStream = new MemoryStream())
					//	{
					//		file.GetStream().CopyTo(memoryStream);
					//		file.Dispose();
					//		tempImage = memoryStream.ToArray();

					//		//DependencyService.Get<DependServices>().ResizeImageAndroid(tempImage, 640, 480);

					//	}
					//	return stream;
					//});


					//DependencyService.Get<DependServices>().setFilePath(_path);
					//DependencyService.Get<DependServices>().AddFileList(_path);
					//imagesList.Add(_path);


					//allFile.Add(_path);
					//Status = string.Format("Path: {0}", _path);

					//issue.IsVisible = false;
					//map.IsVisible = false;
					//pic.IsVisible = true;
					////_imageSource = ImageSource.FromStream(() => mediaFile.Source);

					////pic.Source = ImageSource.FromStream(() => mediaFile.Source);

					//Debug.WriteLine("source image:");
					////Debug.WriteLine(_imageSource.ToString());
					//Debug.WriteLine(_path);



				}




				issue.IsVisible = false;
				map.IsVisible = false;
			};
			btnattach.GestureRecognizers.Add(tap);

			var tapIssue = new TapGestureRecognizer();
			tapIssue.Tapped += (sender, e) =>
			{
				pic.IsVisible = false;
				issue.IsVisible = true;
				map.IsVisible = false;
				labelFile.IsVisible = false;
				labelTitle.IsVisible = false;
			};
			btnissue.GestureRecognizers.Add(tapIssue);

			var tapMap = new TapGestureRecognizer();
			tapMap.Tapped += async (sender, e) =>
			{
				labelFile.IsVisible = false;
				labelTitle.IsVisible = false;
				pic.IsVisible = false;
				issue.IsVisible = false;
				map.IsVisible = true;
				if (_geolocator == null)
				{
					await GetPosition();
				}
				Debug.WriteLine("La final: " + latitude);
				Debug.WriteLine("Lo final: " + longitude);

				//map.MoveToRegion(new MapSpan(new Xamarin.Forms.Maps.Position(latitude,longitude),0.01,0.01));
				//				map.MoveToRegion(MapSpan.FromCenterAndRadius(new Xamarin.Forms.Maps.Position(latitude,longitude),Distance.FromMiles(0.3)));
				//				var pin = new Pin{
				//					Type = PinType.Place,
				//					Position = new Xamarin.Forms.Maps.Position(latitude,longitude),
				//					Label = "Your Current Position"
				//				};
				//				map.Pins.Add(pin);

			};
			btnlocation.GestureRecognizers.Add(tapMap);

			var tapRecord = new TapGestureRecognizer();
			tapRecord.Tapped += (sender, e) => {

				//await DisplayAlert("info", "just inform you","Ok");
				btnRecord.IsVisible = false;
				btnStopRecord.IsVisible = true;

				DateTime now = DateTime.Now.ToLocalTime();
				if (DateTime.Now.IsDaylightSavingTime() == true)
				{
					now = now.AddHours(1);
				}

				String time = string.Format("rec_{0}", now.ToString("yy-MM-dd"));

				Debug.WriteLine(time);

				DependencyService.Get<DependServices>().RecordAudio(time + ".3gpp" );

			};

			btnRecord.GestureRecognizers.Add(tapRecord);

			var tapStopRecord = new TapGestureRecognizer();
			tapStopRecord.Tapped += (sender, e) => { 
				btnRecord.IsVisible = true;
				btnStopRecord.IsVisible = false;
				DependencyService.Get<DependServices>().StopRecord();

			};

			btnStopRecord.GestureRecognizers.Add(tapStopRecord);


			//toolbaritem
			//			var attachFile = new ToolbarItem("Attach File", "", () => {
			//				//function
			//				getFile();
			//			}, 0, 0);
			//			attachFile.Order = ToolbarItemOrder.Secondary;
			//			ToolbarItems.Add (attachFile);
			//
			//			var attachImage = new ToolbarItem ("Attach Image", "", async() => {
			//				await TakePicture();
			//				pic.IsVisible = true;
			//				issue.IsVisible = false;
			//				map.IsVisible = false;
			//			}, 0, 0);
			//			ToolbarItems.Add (attachImage);
			//			attachImage.Order = ToolbarItemOrder.Secondary; // forces it to appear in menu on Android
			//
			//			var attachLocation = new ToolbarItem ("Attach Location", "", async() => {
			//				pic.IsVisible = false;
			//				issue.IsVisible = false;
			//				map.IsVisible = true;
			//				if (_geolocator == null)
			//				{
			//					await GetPosition();
			//				}
			//				Debug.WriteLine("La final: " + latitude);
			//				Debug.WriteLine("Lo final: " +longitude);
			//			}, 0, 0);
			//			ToolbarItems.Add (attachLocation);
			//			attachLocation.Order = ToolbarItemOrder.Secondary; // forces it to appear in menu on Android

			if (Device.Idiom == TargetIdiom.Phone)
			{
				name.WidthRequest = 150;
				email.WidthRequest = 150;
				tcdlogo.WidthRequest = 140;

				StackLayout mainFormStackLayout = new StackLayout
				{
					Padding = 10,
					Children = {
						new StackLayout
						{
							Orientation = StackOrientation.Horizontal,
							VerticalOptions = LayoutOptions.Fill ,
							//HorizontalOptions = LayoutOptions.FillAndExpand ,
							Children = {
								new StackLayout
								{
									Orientation = StackOrientation.Vertical,
									Children =
									{
										name,
										email
									}
									},
								tcdlogo
							}
						},
						phone,
						company,
						subject,
						issue,
						pic,
						map,
						labelTitle,
						labelFile,
						new StackLayout
						{

							Orientation = StackOrientation.Horizontal,
							Children = {

								new StackLayout
								{
									Spacing = 0,
									Padding = 0,
									Orientation = StackOrientation.Horizontal,
									HorizontalOptions = LayoutOptions.StartAndExpand,
									Children ={isremember}
								},
								new StackLayout
								{
									Spacing = 0,
									Padding = 0,
									Orientation = StackOrientation.Horizontal,
									HorizontalOptions = LayoutOptions.StartAndExpand,
									Children ={btnattach, btnlocation, btnissue, btnRecord,btnStopRecord,indicator}
								}
								//btnattach, btnlocation,btnissue
							}
						},

						submit
					}


				};

				Content = new ScrollView
				{
					Content = mainFormStackLayout
				};

			}
			else {
				// for tablet
				name.WidthRequest = 350;
				email.WidthRequest = 350;
				tcdlogo.WidthRequest = 140;
				Content = new StackLayout
				{
					Padding = 10,
					Children = {
						new StackLayout
						{
							Orientation = StackOrientation.Horizontal,
							VerticalOptions = LayoutOptions.Fill ,
							//HorizontalOptions = LayoutOptions.FillAndExpand ,
							Children = {
								new StackLayout
								{
									Orientation = StackOrientation.Vertical,
									Children =
									{
										name,
										email
									}
									},
								tcdlogo
							}
						},
						phone,
						company,
						subject,
						issue,
						pic,
						map,
						new StackLayout
						{

							Orientation = StackOrientation.Horizontal,
							Children = {

								new StackLayout
								{
									Spacing = 0,
									Padding = 0,
									Orientation = StackOrientation.Horizontal,
									HorizontalOptions = LayoutOptions.StartAndExpand,
									Children ={isremember}
								},
								new StackLayout
								{
									Spacing = 0,
									Padding = 0,
									Orientation = StackOrientation.Horizontal,
									HorizontalOptions = LayoutOptions.StartAndExpand,
									Children ={btnattach, btnlocation, btnissue}
								}
								//btnattach, btnlocation,btnissue
							}
						},

						submit
					}
				};

			}


			if (TCDButtonCrossPlatform.Helpers.Settings.UserSave == "true")
			{
				isremember.IsToggled = true;
				name.Text = TCDButtonCrossPlatform.Helpers.Settings.UserName;
				email.Text = TCDButtonCrossPlatform.Helpers.Settings.UserEmail;
				company.Text = TCDButtonCrossPlatform.Helpers.Settings.UserCompany;
				phone.Text = TCDButtonCrossPlatform.Helpers.Settings.UserPhone;
			}
			else {
				isremember.IsToggled = false;
			}
			//send ticket
			submit.Clicked += async (object sender, EventArgs e) =>
		  {
			  //Debug.WriteLine("Total file : " + fileList.Count);
			  

			  cts = new CancellationTokenSource();

			  string ahstr = DateTime.Now.ToString("HH");
			  string ah = "No";
			  int timeNow = Int32.Parse(ahstr);

			  if (timeNow < 9 || timeNow > 17)
			  {




				  var answer = await DisplayAlert("Office Hour", "Do you need after office hour support ?", "Yes", "No");

				  //var answer = await DisplayAlert("Office Hour", "Do you need after office hour support ?", "Yes", "No");
				  Debug.WriteLine("Answer: " + answer);

				  if (answer == true)
				  {

					  ah = "Yes";
					  doSubmit(ah, cts.Token);
					  //var taskSubmit = Task.Factory.StartNew(async () =>
					  // {
					  //  await doSubmit(ah, cts.Token);

					  // });


				  }
				  else {
					  await DisplayAlert("Info", "You have Cancelled a ticket", "Ok");
				  }
			  }
			  else {

				  doSubmit(ah, cts.Token);
				  //var taskSubmit = Task.Factory.StartNew(async () =>
				  // {
				  //  doSubmit(ah, cts.Token);

				  // });


			  }


		  };
			isremember.Toggled += (object sender, ToggledEventArgs e) =>
			{
				if (isremember.IsToggled)
				{
					TCDButtonCrossPlatform.Helpers.Settings.UserSave = "true";
				}
				else {
					TCDButtonCrossPlatform.Helpers.Settings.UserSave = "false";
				}
			};
			Status = "Ready";
		}

		/// <summary>
		/// Setups this instance.
		/// </summary>
		private void SetupCamera()
		{
			if (_mediaPicker != null)
			{
				return;
			}

			_device = Resolver.Resolve<IDevice>();

			////RM: hack for working on windows phone?
			_mediaPicker = Resolver.Resolve<IMediaPicker>() ?? _device.MediaPicker;
		}

		/// <summary>
		/// Takes the picture. This is a variation from the CameraPage example.
		/// In this version, we need to return the Path to the file, since we need
		/// that path to attach it to the email.
		/// </summary>
		/// <returns>Take Picture Task.</returns>
		private async Task<MediaFile> TakePicture()
		{
			SetupCamera();

			return await _mediaPicker.TakePhotoAsync(new CameraMediaStorageOptions { DefaultCamera = CameraDevice.Front, MaxPixelDimension = 400 }).ContinueWith(t =>
				{
					if (t.IsFaulted)
					{
						Status = t.Exception.InnerException.ToString();
					}
					else if (t.IsCanceled)
					{
						Status = "Canceled";
					}
					else
					{
						var mediaFile = t.Result;
						

						_path = mediaFile.Path;
						//DependencyService.Get<DependServices>().setFilePath(_path);
						DependencyService.Get<DependServices>().AddFileList(_path);
						imagesList.Add(_path);
						
						
						allFile.Add(_path);
						Status = string.Format("Path: {0}", _path);
						issue.IsVisible = false;
						map.IsVisible = false;
						pic.IsVisible = true;
						_imageSource = ImageSource.FromStream(() => mediaFile.Source);
						
						pic.Source = ImageSource.FromStream(() => mediaFile.Source);
						
						Debug.WriteLine("source image:");
						Debug.WriteLine(_imageSource.ToString());
						Debug.WriteLine(_path);

						return mediaFile;
					}

					return null;
				}, _scheduler);
		}

		/// <summary>
		/// Gets the position.
		/// </summary>
		/// <returns>Task.</returns>

		private async Task GetPosition()
		{
			_cancelSource = new CancellationTokenSource();
			indicator.IsRunning = true;
			PositionStatus = string.Empty;
			PositionLatitude = string.Empty;
			PositionLongitude = string.Empty;
			longitude = 0.0;
			latitude = 0.0;
			IsBusy = true;
			await
			Geolocator.GetPositionAsync(10000, _cancelSource.Token, true)
				.ContinueWith(t =>
					{
						IsBusy = false;
						if (t.IsFaulted)
						{
							PositionStatus = ((GeolocationException)t.Exception.InnerException).Error.ToString();
						}
						else if (t.IsCanceled)
						{
							PositionStatus = "Canceled";
						}
						else
						{
							PositionStatus = t.Result.Timestamp.ToString("G");
							PositionLatitude = "La: " + t.Result.Latitude.ToString("N4");
							PositionLongitude = "Lo: " + t.Result.Longitude.ToString("N4");
							Debug.WriteLine("La: " + t.Result.Latitude.ToString("N4"));
							Debug.WriteLine("Lo: " + t.Result.Longitude.ToString("N4"));
							longitude = t.Result.Longitude;
							latitude = t.Result.Latitude;
							map.MoveToRegion(new MapSpan(new Xamarin.Forms.Maps.Position(latitude, longitude), 0.01, 0.01));
							//map.MoveToRegion(MapSpan.FromCenterAndRadius(new Xamarin.Forms.Maps.Position(latitude,longitude),Distance.FromMiles(0.3)));
							var pin = new Pin
							{
								Type = PinType.Place,
								Position = new Xamarin.Forms.Maps.Position(latitude, longitude),
								Label = "Your Current Position"
							};
							map.Pins.Add(pin);
							longlat = "Latitude: " + latitude.ToString() + "\n" + "Longitude: " + longitude.ToString() + "\n";
							//							map =  new Map(
							//								MapSpan.FromCenterAndRadius(
							//									new Xamarin.Forms.Maps.Position(latitude,longitude), Distance.FromMiles(0.3))) {
							//								VerticalOptions = LayoutOptions.FillAndExpand,
							//								HeightRequest = 180,
							//								BackgroundColor = Color.Blue,
							//
							//							};
							indicator.IsRunning = false;

						}
					}, _scheduler);
		}

		/// <summary>
		/// Gets the geolocator.
		/// </summary>
		/// <value>The geolocator.</value>
		private IGeolocator Geolocator
		{
			get
			{
				if (_geolocator == null)
				{
					_geolocator = DependencyService.Get<IGeolocator>();
					_geolocator.PositionError += OnListeningError;
					_geolocator.PositionChanged += OnPositionChanged;
				}
				return _geolocator;
			}
		}

		private void OnListeningError(object sender, PositionErrorEventArgs e)
		{
			////			BeginInvokeOnMainThread (() => {
			////				ListenStatus.Text = e.Error.ToString();
			////			});
		}

		private void OnPositionChanged(object sender, PositionEventArgs e)
		{


			////			BeginInvokeOnMainThread (() => {
			////				ListenStatus.Text = e.Position.Timestamp.ToString("G");
			////				ListenLatitude.Text = "La: " + e.Position.Latitude.ToString("N4");
			////				ListenLongitude.Text = "Lo: " + e.Position.Longitude.ToString("N4");
			////			});
			///
		}

		protected async Task<string> getJobID(string autoCollect, string userDefined, int tick)
		{
			try
			{
				//			System.Net.WebRequest s = default(System.Net.WebRequest);
				string jobid = "";
				string url = "http://www.tcd.com.au/TCDSupport/EventsSubmit.asp?";
				string purl = string.Format("{4}{1}{0}{2}{0}{3}", "&", "eventDes=" + autoCollect, "rmarks=" + userDefined, "tick=" + tick.ToString(), url);
				//			s = System.Net.WebRequest.Create(request);
				//			s.Method = "GET";
				//			System.Net.WebResponse response = s.GetResponse() ;
				//			System.IO.StreamReader parse = new System.IO.StreamReader(response.GetResponseStream());
				HttpWebRequest request = default(HttpWebRequest);
				request = (HttpWebRequest)WebRequest.Create(purl);
				using (var response = (HttpWebResponse)(await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, null)))
				{
					StreamReader reader = new StreamReader(response.GetResponseStream());
					jobid = reader.ReadToEnd();
					Debug.WriteLine(jobid);
					return jobid;
					//					string Id = Guid.NewGuid().ToString();
					//					return Id;
				}
			}
			catch (Exception e)
			{
				string Id = Guid.NewGuid().ToString();
				return Id;
			}



		}

		public async void sendMailService(string jobid, string name, string email, string company, string subject, string issue, string phone, string fileType)
		{
			string url = "http://www.tcd.com.au/TCDSupport/SendEmail.asp";
			string att = "";


			//if (imagesList.Count == 1)
			//{
			//	att = WebUtility.UrlEncode("jpg");
			//}
			//else if (imagesList.Count > 1)
			//{
			//	att = "zip";
			//}

			//if (fileList.Count == 1)
			//{
			//	if (fileType != "")
			//		att = fileType;
			//}
			//else if (fileList.Count > 1)
			//{
			//	att = "zip";
			//}


			if (allFile.Count == 1)
			{
				if (fileType != "")
					att = fileType;
			}
			else if (allFile.Count > 1)
			{
				att = "zip";
			}


			string UserCollection = "Name: " + name + System.Environment.NewLine + "Number: " + phone + System.Environment.NewLine + "Company Name: " + company + System.Environment.NewLine + "OS: " + Device.OS.ToString() + System.Environment.NewLine + longlat;
			string Message = string.Format("{0}{1}{1}Additional information{1}{1}{2}", issue, System.Environment.NewLine, UserCollection);
			string data = string.Format("TCDID={0}&email={1}&eventDes={2}&rmarks={3}&Scr={4}&att={5}", WebUtility.UrlEncode("{" + jobid + "}"), WebUtility.UrlEncode(email), WebUtility.UrlEncode(subject), WebUtility.UrlEncode(Message), "", att);
			byte[] byteArray = Encoding.UTF8.GetBytes(data);

			HttpWebRequest request = default(HttpWebRequest);
			request = (HttpWebRequest)WebRequest.Create(url);
			request.Method = "POST";
			request.ContentType = "application/x-www-form-urlencoded";

			using (var postStream = await Task.Factory.FromAsync<Stream>(request.BeginGetRequestStream, request.EndGetRequestStream, null))
			{
				await postStream.WriteAsync(byteArray, 0, byteArray.Length);
			}

			try
			{
				var response = (HttpWebResponse)await Task.Factory.FromAsync<WebResponse>(request.BeginGetResponse, request.EndGetResponse, null);
				if (response != null)
				{
					var reader = new StreamReader(response.GetResponseStream());
					// ASYNC: using StreamReader's async method to read to end, in case
					// the stream i slarge.
					string responseString = await reader.ReadToEndAsync();
					Debug.WriteLine("good response");
					Debug.WriteLine(responseString);
				}
			}
			catch (WebException we)
			{
				var reader = new StreamReader(we.Response.GetResponseStream());
				string responseString = reader.ReadToEnd();
				Debug.WriteLine("bad response");
				Debug.WriteLine(responseString);
			}

		}

		protected string getPhoneNumber()
		{
			//			if(Device.OS == TargetPlatform.Android){
			//
			//			}
			//			string number = DependencyService.Get<DependServices> ().getPhoneNumberFromSim ();
			string number = TCDButtonCrossPlatform.Helpers.Settings.UserPhone;


			Debug.WriteLine("numbernya :");
			Debug.WriteLine(number);
			return number;
		}

		protected string getFile()
		{
			string filePath = DependencyService.Get<DependServices>().getFile();

			//fileList.Add(filePath);
			allFile.Add(filePath);
			return filePath;
		}

		protected int getTick()
		{
			Random rn = new Random();
			return rn.Next(0, 65535);
		}

		void resetParameter()
		{
			fileList = null;
			imagesList = null;
			tempChooseDialog = "";



		}

		async void doSubmit(string ah, CancellationToken ct)
		{
			indicator.IsRunning = false;
			if (isremember.IsToggled)
			{
				TCDButtonCrossPlatform.Helpers.Settings.UserName = name.Text;
				TCDButtonCrossPlatform.Helpers.Settings.UserEmail = email.Text;
				TCDButtonCrossPlatform.Helpers.Settings.UserCompany = company.Text;
				TCDButtonCrossPlatform.Helpers.Settings.UserPhone = phone.Text;
			}


			string[] tempArrList = DependencyService.Get<DependServices>().getFileList().ToArray();
			foreach (string item in tempArrList)
			{
				Debug.WriteLine("hallo");
				Debug.WriteLine(item);
			}




			Dictionary<string, string> details = new Dictionary<string, string>();
			if (name.Text != "" && email.Text != "" && company.Text != "" && subject.Text != "" && issue.Text != "" && phone.Text != "")
			{
				string test = "";
				details.Add("name:", name.Text);
				details.Add("contact:", phone.Text);
				details.Add("company:", company.Text);
				details.Add("email:", email.Text);
				int count = details.Count;
				int i = 0;

				foreach (KeyValuePair<string, string> detail_loopVariable in details)
				{
					if (i == 0)
					{
						test = "[";
					}
					test += "\"" + detail_loopVariable.Key + " " + detail_loopVariable.Value + "\"";
					if (i == (count - 1))
					{
						test += "]";
					}
					else {
						test += ",";
						i++;
					}

				}





				string JobId = Task.Run(() => getJobID(test, issue.Text, getTick())).Result;
				string resp1 = Task.Run(() => ws.InsertTicket(_device.Id, "Android", JobId, subject.Text, issue.Text, company.Text, email.Text, phone.Text, latitude.ToString(), longitude.ToString(), DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), ah)).Result;
				string resp2 = Task.Run(() => ws.InsertUser_Details(name.Text, _device.Id, company.Text, email.Text, phone.Text)).Result;

				Debug.WriteLine(JobId);


				Debug.WriteLine(resp1);
				Debug.WriteLine(resp2);

				if (resp1.Equals("ERROR") || resp2.Equals("ERROR"))
				{
					await DisplayAlert("Info", "Server Down", "Ok");
					Debug.WriteLine("response error, connection failed");

				}
				else {

					if (resp1.ToLower().Contains("success"))
					{
						if (resp2.ToLower().Contains("success"))
						{
							allFile = DependencyService.Get<DependServices>().getFileList();
							//Debug.WriteLine("cek imagelist = " + imagesList.Count);
							//Debug.WriteLine("cek filelist = " + fileList.Count);
							Debug.WriteLine("Check All files = " + allFile.Count);
							Debug.WriteLine(allFile.ToArray());
							//Debug.WriteLine(imagesList.ToArray());
							//Debug.WriteLine(fileList.ToArray());



							string typeFile = "";
							string fileDoc = "";


							//if (imagesList.Count == 1)
							//{
							//	DependencyService.Get<DependServices>().uploadFile(_path, JobId, "jpg");

							//}
							//else if (imagesList.Count > 1)
							//{

							//	string fileZip = DependencyService.Get<DependServices>().ZipFile(imagesList.ToArray(), JobId + ".zip");
							//	Debug.WriteLine(fileZip);


							//	DependencyService.Get<DependServices>().uploadFile(fileZip, JobId, "zip");
							//}

							//fileList = DependencyService.Get<DependServices>().getFileList();


							//if (fileList.Count == 1)
							//{
							//	typeFile = DependencyService.Get<DependServices>().getTypeFile();
							//	fileDoc = DependencyService.Get<DependServices>().getFilePath();

							//	DependencyService.Get<DependServices>().uploadFile(fileDoc, JobId, typeFile);

							//}
							//else if (fileList.Count > 1)
							//{


							//	string fileZip = DependencyService.Get<DependServices>().ZipFile2(DependencyService.Get<DependServices>().getFileList().ToArray(), JobId + ".zip");

							//	DependencyService.Get<DependServices>().uploadFile(fileZip, JobId, "zip");


							//}


							if (allFile.Count == 1)
							{
								//typeFile = DependencyService.Get<DependServices>().getTypeFile();
								//fileDoc = DependencyService.Get<DependServices>().getFilePath();

								fileDoc = allFile[0];
								var temp = fileDoc.Split('.');
								typeFile = temp[temp.Length - 1];

								DependencyService.Get<DependServices>().uploadFile(fileDoc, JobId, typeFile);

							}
							else if(allFile.Count > 1) {
								string fileZip = DependencyService.Get<DependServices>().ZipFile2(DependencyService.Get<DependServices>().getFileList().ToArray(), JobId + ".zip");
								DependencyService.Get<DependServices>().uploadFile(fileZip, JobId, "zip");
							}

							sendMailService(JobId, name.Text, email.Text, company.Text, subject.Text, issue.Text, phone.Text, typeFile);



							await DisplayAlert("Info", "Your Ticket has been submited", "Ok");


							indicator.IsRunning = false;

							//clear variable
							resetParameter();
							pic.Source = "";
							issue.Text = "";
							subject.Text = "";
							DependencyService.Get<DependServices>().ClearFileList();
							//cts = null;

						}
					}
				}

			}
			else {
				await DisplayAlert("Attention", "Fields not allowed empty", "Ok");
			}//end validation



		}

	}
}


