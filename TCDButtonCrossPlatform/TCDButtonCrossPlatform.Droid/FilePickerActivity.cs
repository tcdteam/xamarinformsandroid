﻿namespace TCDButtonCrossPlatform.Droid
{
	using Android.App;
	using Android.OS;
	using Android.Support.V4.App;

	[Activity(Label = "TCDButtonCrossPlatform", MainLauncher = true, Icon = "@drawable/ic_launcher")]
	public class FilePickerActivity : FragmentActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.Main);
		}
	}
}