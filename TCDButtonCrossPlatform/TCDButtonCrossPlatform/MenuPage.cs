﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace TCDButtonCrossPlatform.Views
{
    public class MenuPage : ContentPage
    {
        public ListView Menu { get; set; }

        public MenuPage()
        {
            Icon = "settings.png";
            Title = "menu"; // The Title property must be set.
            //BackgroundColor = Color.FromHex("333333");
			BackgroundColor = Device.OnPlatform (Color.FromHex("333333"), Color.Default, Color.Default);

            Menu = new MenuListView();
			Menu.RowHeight = 50;
            var menuLabel = new ContentView
            {
                Padding = new Thickness(5, 15, 5, 15),
                Content = new Label
                {
					TextColor = Color.White,
                    Text = "MENU",
                }
            };


            var layout = new StackLayout
            {
                Spacing = 0,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            layout.Children.Add(menuLabel);
			layout.Children.Add(new BoxView() { Color = Color.FromHex("AAAAAA"), WidthRequest = 100, HeightRequest = 2 });
            layout.Children.Add(Menu);

            Content = layout;
          
        }
    }
}
