﻿using System;
using Android.Telephony;
using TCDButtonCrossPlatform.Droid;
using Android.Content;
using System.Net;
using System.Diagnostics;
using Java.Util.Zip;
using Xamarin.Forms;
using Android.App;
using Android.Content.PM;
using System.Collections.Generic;
using Android.Media;
using System.IO;
using Android.Graphics;

[assembly: Xamarin.Forms.Dependency(typeof(AndroidServices))]
namespace TCDButtonCrossPlatform.Droid
{
	public class AndroidServices : DependServices
	{
		private string pathFile;
		List<string> fileList = new List<string> ();


		public string getPhoneNumberFromSim (){
			TelephonyManager mTelephonyMgr;

			mTelephonyMgr = (TelephonyManager)Android.App.Application.Context.GetSystemService(Android.App.Application.TelephonyService);

			var Number = mTelephonyMgr.Line1Number;
			return Number;
		}

		public void uploadFile (string path, string jobId, string type){
			using(WebClient client = new WebClient())
			{
				try
				{

					NetworkCredential myCreds = new NetworkCredential("tridgeon", "tcd#w3ndy$");
					client.Credentials = myCreds;
					Debug.WriteLine("before upload path : "+path);
					client.UploadFile("http://www.tcd.com.au/TCDSupport/Upload/" + "Att-{" + jobId + "}."+type , "PUT", path);
					Debug.WriteLine("http://www.tcd.com.au/TCDSupport/Upload/" + "Att-{" + jobId + "}."+type);

				}
				catch (Exception ex) {
					Debug.WriteLine (ex.ToString());
				}
			}
		}

		public void setFilePath(String path) {
			this.pathFile = path;
		}

		public string getFilePath(){
			return pathFile;
		}

		public string getTypeFile(){
			if (pathFile != "") {
				var temp = pathFile.Split ('.');
				return temp[temp.Length-1];
			} else {
				return "";
			}
		}

		public List<string> getFileList(){
			return fileList;	
		}

		public string getFile()
		{
//			var intent = new Intent (Forms.Context, typeof(FilePickerActivity));
//			Forms.Context.StartActivity(typeof(FilePickerActivity));

//			var intents = new Intent (Intent.ActionGetContent);
//			intents.SetType ("documents/*");
//			intents.SetAction (Intent.ActionGetContent);
//			Forms.Context.StartActivity(Intent.CreateChooser(intents, "Choose Video"));

			var mainActivity = Forms.Context as MainActivity;
			var intents = new Intent (Intent.ActionGetContent);
			intents.SetType ("documents/*");
			intents.SetAction (Intent.ActionGetContent);


			mainActivity.StartActivity(intents, OnActivityResult);


			return pathFile;
		}

		private void OnActivityResult(int requestCode, Result resultCode, Intent data){

			if (data != null) {
				Android.Net.Uri uri = data.Data;
				Debug.WriteLine (uri);
				pathFile = uri.Path;

				AddFileList(pathFile);

				Debug.WriteLine (pathFile);
				Debug.WriteLine (data);
			}

		}

		public void AddFileList(String pathFile) { 
			if (!fileList.Contains(pathFile))
			{
				fileList.Add(pathFile);
				Debug.WriteLine("Added file: " + pathFile);
			}
		
		}

		public void ClearFileList() {
			fileList.Clear();
		}


		public string ZipFile(string[] arrFiles, string sZipFileName)
		{
			Debug.WriteLine (string.Join(",",arrFiles));
			string sZipToDirectory = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			if (System.IO.Directory.Exists(sZipToDirectory))
			{
				System.IO.FileStream fNewZipFileStream;
				ZipOutputStream zos;

				try {
					fNewZipFileStream = System.IO.File.Create(sZipToDirectory+"/"+ sZipFileName);
					zos = new ZipOutputStream(fNewZipFileStream);

					for (int i = 0; i < arrFiles.Length; i++) {
						string ent = Android.Net.Uri.Parse(arrFiles[i].Substring(arrFiles[i].LastIndexOf("/") + 1)).ToString();
						ZipEntry entry = new ZipEntry(ent);
						Debug.WriteLine(ent);
						zos.PutNextEntry(entry);
						byte[] fileContents = System.IO.File.ReadAllBytes(arrFiles[i]);
						zos.Write(fileContents);
						zos.CloseEntry();

						//						System.IO.FileStream fStream = System.IO.File.OpenRead(arrFiles[i]);
						//						System.IO.BufferedStream bfStrm = new System.IO.BufferedStream(fStream);
						//
						//						byte[] buffer = new byte[bfStrm.Length];
						//						int count;
						//						while ((count = bfStrm.Read(buffer, 0, 1024)) != -1) {
						//							zos.Write(buffer);
						//						}
						//
						//						bfStrm.Close();
						//						fStream.Close();
						//						zos.CloseEntry();
					}
					zos.Close();
					fNewZipFileStream.Close();
					return sZipToDirectory+"/"+sZipFileName;
				}
				catch (Exception ex)
				{
					string sErr = ex.Message;
					return "error";
				}
				finally
				{
					fNewZipFileStream = null;
					zos = null;
				}
			}
			else
			{
				return "error";
			}
		}

		public string ZipFile2(string[] arrFiles, string sZipFileName)
		{
			string sZipToDirectory = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			if (System.IO.Directory.Exists(sZipToDirectory))
			{
				System.IO.FileStream fNewZipFileStream;
				ZipOutputStream zos;

				try {
					fNewZipFileStream = System.IO.File.Create(sZipToDirectory+"/"+ sZipFileName);
					zos = new ZipOutputStream(fNewZipFileStream);

					foreach (var item in arrFiles) {
						Debug.WriteLine(item);
						string ent = Android.Net.Uri.Parse(item.Substring(item.LastIndexOf("/") + 1).ToString()).ToString();
						Debug.WriteLine("before adding as entry to zip : "+ent);
						ZipEntry entry = new ZipEntry(ent);

						zos.PutNextEntry(entry);
						byte[] fileContents = System.IO.File.ReadAllBytes(item);
						zos.Write(fileContents);
						zos.CloseEntry();

						//						System.IO.FileStream fStream = System.IO.File.OpenRead(arrFiles[i]);
						//						System.IO.BufferedStream bfStrm = new System.IO.BufferedStream(fStream);
						//
						//						byte[] buffer = new byte[bfStrm.Length];
						//						int count;
						//						while ((count = bfStrm.Read(buffer, 0, 1024)) != -1) {
						//							zos.Write(buffer);
						//						}
						//
						//						bfStrm.Close();
						//						fStream.Close();
						//						zos.CloseEntry();
					}
					zos.Close();
					fNewZipFileStream.Close();
					return sZipToDirectory+"/"+sZipFileName;
				}
				catch (Exception ex)
				{
					string sErr = ex.Message;
					return "error";
				}
				finally
				{
					fNewZipFileStream = null;
					zos = null;
				}
			}
			else
			{
				return "error";
			}
		}

		private MediaRecorder recorder;
		private String filename;

		public void RecordAudio(String file)
		{
			try
			{
				
				String se = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
				var folder = new Java.IO.File(se + "/files");
				//var folder = new Java.IO.File("/sdcard/Android/data/au.com.tcd.TCDButton/files/audio");

				if (!folder.IsDirectory) {
					folder.Mkdirs();
				}
				Debug.WriteLine(folder);
				file = folder + "/" + file;
				this.filename = file;

				//file = "/sdcard/Music/test.3gpp";
				Debug.WriteLine(file);
				if (File.Exists(file))
				{

					File.Delete(file);
				}
				if (this.recorder == null)
				{
					this.recorder = new MediaRecorder(); // Initial state.
					this.recorder.Reset();
					this.recorder.SetAudioSource(AudioSource.Mic);
					this.recorder.SetOutputFormat(OutputFormat.ThreeGpp);
					this.recorder.SetAudioEncoder(AudioEncoder.AmrNb);
					// Initialized state.
					this.recorder.SetOutputFile(file);
					// DataSourceConfigured state.
					this.recorder.Prepare(); // Prepared state
					this.recorder.Start(); // Recording state.
					Debug.WriteLine("Start Recording");
				}
				else {
					this.recorder.Reset();
					this.recorder.SetAudioSource(AudioSource.Mic);
					this.recorder.SetOutputFormat(OutputFormat.ThreeGpp);
					this.recorder.SetAudioEncoder(AudioEncoder.AmrNb);
					// Initialized state.
					this.recorder.SetOutputFile(file);
					// DataSourceConfigured state.
					this.recorder.Prepare(); // Prepared state
					this.recorder.Start(); // Recording state.
					Debug.WriteLine("Start Recording");
				}
				}

			catch (Exception ex)
			{
				Console.Out.WriteLine(ex.StackTrace);

			}
		}



		public void StopRecord() {

			if (this.recorder != null)
			{
				try
				{
					this.recorder.Stop();
					this.recorder.Reset();

					Debug.WriteLine("Stop Recording");
					Debug.WriteLine(filename);
					fileList.Add(filename);
					StartPlayer(filename);

				}
				catch (Exception ex)
				{
					Debug.WriteLine(ex.Message);
				}
				finally { 
					
				}

			}
			else {
				Debug.WriteLine("Sorry you dont have object recorder");
			}



		}

		protected MediaPlayer player;
		public void StartPlayer(String filePath)
		{
			if (this.player == null)
			{
				this.player = new MediaPlayer();
			
				this.player.Reset();
				this.player.SetDataSource(filePath);
				this.player.Prepare();
				this.player.Start();
			}else{ 
				this.player.Reset();
				this.player.SetDataSource(filePath);
				this.player.Prepare();
				this.player.Start();

				this.player.Completion += (sender, e) => {
					StopPlayer();
				
				};
			}
		}

		public void StopPlayer() {
			this.player.Stop();
			this.player.Release();
		}


		public String getFileRecord() {

			return this.filename;
		}


		public static byte[] ResizeImageAndroid(byte[] imageData, float width, float height)
		{
			// Load the bitmap
			Bitmap originalImage = BitmapFactory.DecodeByteArray(imageData, 0, imageData.Length);
			Bitmap resizedImage = Bitmap.CreateScaledBitmap(originalImage, (int)width, (int)height, false);

			using (MemoryStream ms = new MemoryStream())
			{
				resizedImage.Compress(Bitmap.CompressFormat.Jpeg, 100, ms);
				return ms.ToArray();
			}
		}


	}
}

