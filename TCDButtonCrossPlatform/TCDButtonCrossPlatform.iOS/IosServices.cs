﻿using System;
using TCDButtonCrossPlatform.iOS;
using System.Net;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;

[assembly: Xamarin.Forms.Dependency (typeof (IosServices))]
namespace TCDButtonCrossPlatform.iOS
{
	public class IosServices : DependServices
	{
		private string pathFile;
		List<string> fileList = new List<string> ();
		HashSet<string> hashset = new HashSet<string>();

		public string getPhoneNumberFromSim (){
			//this function is disable in IOS because limitation from apple lack securtiy and privacy problem
			// i left this variable keep empty just for formality
			string number = String.Empty;
			return number;
		}

		public string getFile(){
			return "";
		}

		public void uploadFile (string path, string jobId, string type){
			using(WebClient client = new WebClient())
			{
				try
				{

					NetworkCredential myCreds = new NetworkCredential("tridgeon", "tcd#w3ndy$");
					client.Credentials = myCreds;
					client.UploadFile("http://www.tcd.com.au/TCDSupport/Upload/" + "Att-{" + jobId + "}."+type , "PUT", path);
					Debug.WriteLine("http://www.tcd.com.au/TCDSupport/Upload/" + "Att-{" + jobId + "}."+type);

				}
				catch (Exception ex) {
					Debug.WriteLine (ex.ToString());
				}
			}
		}

		public string ZipFile(string[] arrFiles, string sZipFileName)
		{
			string sZipToDirectory = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			if (System.IO.Directory.Exists(sZipToDirectory))
			{
				System.IO.FileStream fNewZipFileStream;
				GZipStream zos;

				try {
					fNewZipFileStream = System.IO.File.Create(sZipToDirectory+"/"+ sZipFileName);
					zos = new GZipStream(fNewZipFileStream,CompressionMode.Compress);

					for (int i = 0; i < arrFiles.Length; i++) {
						FileStream file = File.OpenRead(arrFiles[i].Substring(arrFiles[i].LastIndexOf("/") + 1));
						int theByte = file.ReadByte();
						while(theByte !=-1){
							zos.WriteByte((byte)theByte);
							theByte = file.ReadByte();
						}
					}
					zos.Close();
					fNewZipFileStream.Close();
					return sZipToDirectory+"/"+sZipFileName;
				}
				catch (Exception ex)
				{
					string sErr = ex.Message;
					return "error";
				}
				finally
				{
					fNewZipFileStream = null;
					zos = null;
				}
			}
			else
			{
				return "error";
			}
		}

		public string ZipFile2(string[] arrFiles, string sZipFileName)
		{
			string sZipToDirectory = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			if (System.IO.Directory.Exists(sZipToDirectory))
			{
				System.IO.FileStream fNewZipFileStream;
				GZipStream zos;

				try {
					fNewZipFileStream = System.IO.File.Create(sZipToDirectory+"/"+ sZipFileName);
					zos = new GZipStream(fNewZipFileStream,CompressionMode.Compress);

					for (int i = 0; i < arrFiles.Length; i++) {
						FileStream file = File.OpenRead(arrFiles[i].Substring(arrFiles[i].LastIndexOf("/") + 1));
						int theByte = file.ReadByte();
						while(theByte !=-1){
							zos.WriteByte((byte)theByte);
							theByte = file.ReadByte();
						}
					}
					zos.Close();
					fNewZipFileStream.Close();
					return sZipToDirectory+"/"+sZipFileName;
				}
				catch (Exception ex)
				{
					string sErr = ex.Message;
					return "error";
				}
				finally
				{
					fNewZipFileStream = null;
					zos = null;
				}
			}
			else
			{
				return "error";
			}
		}

		public string getFilePath(){
			return pathFile;
		}

		public string getTypeFile(){
			if (pathFile != "") {
				var temp = pathFile.Split ('.');
				return temp[temp.Length-1];
			} else {
				return "";
			}
		}

		public List<string> getFileList(){
			return fileList;	
		}
	}
}

