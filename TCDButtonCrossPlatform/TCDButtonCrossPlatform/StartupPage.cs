﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;
using TCDButtonCrossPlatform;
using TCDButtonCrossPlatform.Views;

namespace TCDButtonCrossPlatform
{
    public class StartupPage : ContentPage
    {
		
        Image logo = new Image { 
            Aspect = Aspect.AspectFit,
            Source = "tcdlogo.png",
			WidthRequest =200,
			HorizontalOptions = LayoutOptions.Start 
        };
        Image button = new Image
        {
            Aspect = Aspect.AspectFit,
            Source = "button.png",
			WidthRequest =80,
			HorizontalOptions = LayoutOptions.End  
        };

        public StartupPage()
        {
			BackgroundColor = Device.OnPlatform (Color.FromHex("333333"), Color.Default, Color.Default);
			this.Title = "TCD Support";
			//this.BackgroundImage = "splashscreenback.png";
            var tap = new TapGestureRecognizer();
            tap.Tapped += (s,e) =>{
                Navigation.PushModalAsync(new MasterMenuPage());
            };
            button.GestureRecognizers.Add(tap);

            Content = new StackLayout
            {
				Padding = 10,
				//BackgroundColor = Color.Black,
				Orientation = StackOrientation.Horizontal,
                VerticalOptions = LayoutOptions.CenterAndExpand,
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                
                Children = {
					logo,
                    button
				}
                    
            };
        }
    }
}
