﻿using System;
using System.Collections.Generic;

namespace TCDButtonCrossPlatform
{
	public interface DependServices
	{
		string getPhoneNumberFromSim();
		void uploadFile(string path,string jobid, string type);
		string ZipFile (string[] arrFiles, string sZipFileName);
		string ZipFile2 (string[] arrFiles, string sZipFileName);
		string getFile();
		string getFilePath();
		string getTypeFile();
		List<string> getFileList();
		void RecordAudio(string path);
		void StopRecord();
		void StartPlayer(string path);
		void StopPlayer();
		string getFileRecord();
		void setFilePath(String path);
		void AddFileList(String file);
		void ClearFileList();
		//byte[] ResizeImageAndroid(byte[] temp, float width, float height);
	}
}

