﻿using System;
using Xamarin.Forms;

namespace TCDButtonCrossPlatform
{
	public class TicketHistoryCell : ViewCell
	{
		public TicketHistoryCell ()
		{
			var ticketLayout = CreateTicketLayout();

			var viewLayout = new StackLayout()
			{
				Orientation = StackOrientation.Horizontal,
				Children = {ticketLayout }
			};
			View = viewLayout;
		}

		static StackLayout CreateTicketLayout()
		{

			var Id = new Label
			{
				HorizontalOptions= LayoutOptions.Start,
				FontSize = Device.GetNamedSize(NamedSize.Small,typeof(Label)),
			};
			Id.SetBinding(Label.TextProperty, "Id");
			Id.IsVisible = false;

			var titleLabel = new Label
			{
				HorizontalOptions= LayoutOptions.FillAndExpand,
				FontSize = Device.GetNamedSize(NamedSize.Small,typeof(Label)),
				FontAttributes = FontAttributes.Bold,

			};
			titleLabel.SetBinding(Label.TextProperty, "Ticket_subject");

			var subjectLabel = new Label
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				FontSize = Device.GetNamedSize(NamedSize.Small,typeof(Label))
			};
			subjectLabel.SetBinding(Label.TextProperty, "Ticket_detail");

			var nameLayout = new StackLayout()
			{
				Padding = new Thickness(20,20,20,20),
				HorizontalOptions = LayoutOptions.StartAndExpand,
				Orientation = StackOrientation.Vertical,
				Children = { Id,titleLabel, subjectLabel }
			};
			return nameLayout;
		}
	}
}

