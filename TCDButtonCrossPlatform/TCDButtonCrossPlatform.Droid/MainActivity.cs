﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using XLabs.Ioc;
using XLabs.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using XLabs.Platform.Device;
using XLabs.Platform.Services;
using XLabs.Platform.Mvvm;
using Android.Content;

namespace TCDButtonCrossPlatform.Droid
{
    [Activity(Label = "TCDButtonCrossPlatform", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	//public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
	public class MainActivity : XFormsApplicationDroid
    {
		

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

			if (!Resolver.IsSet)
			{
				this.SetIoc();
			}
			else
			{
				var app = Resolver.Resolve<IXFormsApp>() as IXFormsApp<XFormsApplicationDroid>;
				app.AppContext = this;
			}

            //global::Xamarin.Forms.Forms.Init(this, bundle);
			Forms.Init(this, bundle);
			Xamarin.FormsMaps.Init(this,bundle);

			LoadApplication(new App());
        }

		private void SetIoc()
		{
			var resolverContainer = new SimpleContainer();

			var app = new XFormsAppDroid();

			app.Init(this);

			var documents = app.AppDataDirectory;


			resolverContainer.Register<IDevice>(t => AndroidDevice.CurrentDevice)
				.Register<IDisplay>(t => t.Resolve<IDevice>().Display)

//				.Register<IFontManager>(t => new FontManager(t.Resolve<IDisplay>()))
//				.Register<IJsonSerializer, Services.Serialization.JsonNET.JsonSerializer>()
//				.Register<IJsonSerializer, JsonSerializer>()
//				.Register<IEmailService, EmailService>()
//				.Register<IMediaPicker, MediaPicker>()
//				.Register<ITextToSpeechService, TextToSpeechService>()
				.Register<IDependencyContainer>(resolverContainer);
//				.Register<IXFormsApp>(app)
//				.Register<ISecureStorage>(t => new KeyVaultStorage(t.Resolve<IDevice>().Id.ToCharArray()))
//				.Register<ISimpleCache>(
//					t => new SQLiteSimpleCache(new SQLitePlatformAndroid(),
//						new SQLiteConnectionString(pathToDatabase, true), t.Resolve<IJsonSerializer>()));


			Resolver.SetResolver(resolverContainer.GetResolver());
		}

		private Action<int, Result, Intent> _resultCallback;

		public void StartActivity(Intent intent, Action<int, Result, Intent> resultCallback)
		{
			_resultCallback = resultCallback;

			StartActivityForResult(intent, 0);
		}

		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult (requestCode, resultCode, data);
			if (_resultCallback != null)
			{
				_resultCallback(requestCode, resultCode, data);
				_resultCallback = null;
			}
		}
	}
    }


