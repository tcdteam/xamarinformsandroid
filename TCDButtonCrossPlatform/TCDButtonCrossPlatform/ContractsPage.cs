﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

namespace TCDButtonCrossPlatform
{
    public class ContractsPage : ContentPage
    {
        public ContractsPage()
        {
			BackgroundColor = Device.OnPlatform (Color.FromHex("333333"), Color.Default, Color.Default);
            Title = "TCD Support Button";
		    Icon = "icon.png";
        }
    }
}
