﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using TCDButtonCrossPlatform.Views;

namespace TCDButtonCrossPlatform.Views
{
    public partial class ButtonPage : ContentPage
    {
        public ButtonPage()
        {
            InitializeComponent();
        }
        public void OnButtonClicked(object sender, EventArgs args)
        {
            Navigation.PushAsync(new MainMenuPage());
           
        }
    }
}
