﻿using System;

using Xamarin.Forms;

namespace TCDButtonCrossPlatform
{
	public class ConnectionPage : ContentPage
	{
		Button button = new Button
		{
			Text = "SAVE",
			BackgroundColor = Color.Blue, 
			TextColor = Color.White,
			BorderRadius =0
		};

		public ConnectionPage ()
		{
			BackgroundColor = Device.OnPlatform (Color.FromHex("333333"), Color.Default, Color.Default);
			Title = "Configuration";
			var server = new Entry{ Placeholder = "Server Name/IP Address", HorizontalOptions = LayoutOptions.FillAndExpand };
			server.Text = TCDButtonCrossPlatform.Helpers.Settings.Server;
			Content = new StackLayout { 
				Padding = 10, 
				Children = {
					new StackLayout{	
						Orientation = StackOrientation.Horizontal,
						VerticalOptions = LayoutOptions.Center,
						Children = {
							new Label{Text="http://", FontSize =18, HorizontalOptions = LayoutOptions.FillAndExpand},
							server
						},
															
					},
					button
				}
			};

			button.Clicked += (sender, e) => {
				TCDButtonCrossPlatform.Helpers.Settings.Server = server.Text;
				if(server.Text == string.Empty){
					DisplayAlert("Alert","Server required!","OK");
				}else{
					DisplayAlert("Info","Server has been setup","OK");
				}

			};
		}
	}
}


