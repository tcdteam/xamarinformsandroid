﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using XLabs.Forms;
using XLabs.Forms.Services;
using XLabs.Platform.Services;
using XLabs.Platform.Device;
using System.Diagnostics;
using System.Threading.Tasks;
using Lotz.Xam.Messaging;
using XLabs.Platform.Services.Geolocation;
using System.Threading;
using Plugin.Geolocator;

namespace TCDButtonCrossPlatform
{
	public class ContactPage : ContentPage
	{
		public double longitude{ get; set;}
		public double latitude{ get; set;}
		private readonly TaskScheduler _scheduler = TaskScheduler.FromCurrentSynchronizationContext();
		private IGeolocator _geolocator;
		private CancellationTokenSource _cancelSource;
		private List<JsonStuff.Contact> recipient;
		JsonStuff ws;
		public string PositionStatus {
			get;
			set;
		}

		public string PositionLatitude {
			get;
			set;
		}
		public string PositionLongitude {
			get;
			set;
		}
		public string Status
		{
			get;
			set;
		}

		public ContactPage ()
		{
			BackgroundColor = Device.OnPlatform (Color.FromHex("333333"), Color.Default, Color.Default);
			string url = "http://"+TCDButtonCrossPlatform.Helpers.Settings.Server;
			ws = new JsonStuff (url);
			//Task.Run (() => this.GetPosition ()).Wait ();

			//recipient = ws.GetDynamicsContact(email,timeoffday,longitude.ToString(),latitude.ToString());

			//Debug.WriteLine ("start contactpage");
			Title = "Support Contact";

			Editor message = new Editor() { HeightRequest = 150 };
			Button btnSMS = new Button() { Text = "SEND SMS", BackgroundColor = Color.Blue, BorderRadius = 0 };
			Button btnCall = new Button() { Text = "CALL", BackgroundColor = Color.Blue, BorderRadius = 0 };

			Content = new StackLayout() { 
				Padding = 10,
				Children = {
					message,
					btnSMS,
					btnCall

				}
			};

		btnSMS.Clicked += async (sender, e) =>{
				

				var SmsTask = MessagingPlugin.SmsMessenger;
				if (SmsTask.CanSendSms)
				{
					await getCurrentLocation();
					string email = TCDButtonCrossPlatform.Helpers.Settings.UserEmail;
					string timeoffday = DateTime.Now.Hour.ToString()+
						":"+DateTime.Now.Minute.ToString()+
						":"+DateTime.Now.Second.ToString();
					recipient = Task.Run (() => ws.GetDynamicsContact (email, timeoffday, longitude.ToString (), latitude.ToString ())).Result;
//					SmsTask.SendSms("08998504787", message.Text);
					if(recipient.Count > 0){
						
						SmsTask.SendSms(recipient[0].ContactSMS, message.Text);
					}else{
						await DisplayAlert("Info","Sorry, This service is not available in your area","OK");
					}
				}
			};

			btnCall.Clicked += async (sender, e)  => {
			
			
				var PhoneCallTask = MessagingPlugin.PhoneDialer;

				if (PhoneCallTask.CanMakePhoneCall){
					await getCurrentLocation();
					string email = TCDButtonCrossPlatform.Helpers.Settings.UserEmail;
					string timeoffday = DateTime.Now.Hour.ToString()+
						":"+DateTime.Now.Minute.ToString()+
						":"+DateTime.Now.Second.ToString();
					recipient = Task.Run (() => ws.GetDynamicsContact (email, timeoffday, longitude.ToString (), latitude.ToString ())).Result;
					//PhoneCallTask.MakePhoneCall("08998504787");
					if(recipient.Count>0){						
						PhoneCallTask.MakePhoneCall(recipient[0].ContactPhone);
					}else{
						await DisplayAlert("Info","Sorry, This service is not available in your area","OK");
					}

				}
			};
		}

		async Task getCurrentLocation ()
		{
			var locator = CrossGeolocator.Current;
			locator.DesiredAccuracy = 50;
			var position = await locator.GetPositionAsync (-1);   //

			//this.Position = position;
			//this.Log = new Log {
			latitude = position.Latitude;
			longitude = position.Longitude;

//			map.MoveToRegion(new MapSpan(new Xamarin.Forms.Maps.Position(latitude,longitude),0.01,0.01));
//			//map.MoveToRegion(new MapSpan(new Xamarin.Forms.Maps.Position(-6.21462,106.84513),0.01,0.01));
//			//map.MoveToRegion(MapSpan.FromCenterAndRadius(new Xamarin.Forms.Maps.Position(latitude,longitude),Distance.FromMiles(0.3)));
//			var pin = new Pin{
//				Type = PinType.Place,
//				Position = new Xamarin.Forms.Maps.Position(latitude,longitude),
//				Label = "Your Current Position"
//			};
//			map.Pins.Add(pin);
			//longlat = "Latitude: " + latitude.ToString() + "\n" + "Longitude: " + longitude.ToString()+ "\n" ;
			//DateHour = this.Position.Timestamp
			//};

			//this.Log.Title = "Latitude: " + log.Latitude.ToString () + " \n" + "Longitude: " + log.Longitude.ToString ();
			//this.Log.Description = "às " + log.DataHora;
			//this.Logs.Add (this.Log);
		}

		private async Task GetPosition()
		{
			_cancelSource = new CancellationTokenSource();

			PositionStatus = string.Empty;
			PositionLatitude = string.Empty;
			PositionLongitude = string.Empty;
			longitude = 0.0;
			latitude = 0.0;
			IsBusy = true;
			await Geolocator.GetPositionAsync(10000, _cancelSource.Token, true)
				.ContinueWith(t =>
					{
						IsBusy = false;
						if (t.IsFaulted)
						{
							PositionStatus = ((GeolocationException) t.Exception.InnerException).Error.ToString();
						}
						else if (t.IsCanceled)
						{
							PositionStatus = "Canceled";
						}
						else
						{
							PositionStatus = t.Result.Timestamp.ToString("G");
							PositionLatitude = "La: " + t.Result.Latitude.ToString("N4");
							PositionLongitude = "Lo: " + t.Result.Longitude.ToString("N4");
							Debug.WriteLine("La: " + t.Result.Latitude.ToString("N4"));
							Debug.WriteLine("Lo: " +t.Result.Longitude.ToString("N4"));
							longitude = t.Result.Longitude;
							latitude = t.Result.Latitude;



						}
					}, _scheduler);
		}

		private IGeolocator Geolocator
		{
			get
			{
				if (_geolocator == null)
				{
					_geolocator = DependencyService.Get<IGeolocator>();
					_geolocator.PositionError += OnListeningError;
					_geolocator.PositionChanged += OnPositionChanged;
				}
				return _geolocator;
			}
		}

		private void OnListeningError(object sender, PositionErrorEventArgs e)
		{
			////			BeginInvokeOnMainThread (() => {
			////				ListenStatus.Text = e.Error.ToString();
			////			});
		}

		private void OnPositionChanged(object sender, PositionEventArgs e)
		{


			////			BeginInvokeOnMainThread (() => {
			////				ListenStatus.Text = e.Position.Timestamp.ToString("G");
			////				ListenLatitude.Text = "La: " + e.Position.Latitude.ToString("N4");
			////				ListenLongitude.Text = "Lo: " + e.Position.Longitude.ToString("N4");
			////			});
			/// 
		}

	}
}


