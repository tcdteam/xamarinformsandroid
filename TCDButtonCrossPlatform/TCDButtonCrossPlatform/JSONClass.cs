﻿using System;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Net.Http;


public class JsonStuff
{
	
	public string URLConnectionString;
	private Dictionary<string, object> tokenizedResponse;

	public JsonStuff (string pURLConnectionString)
	{
		URLConnectionString = pURLConnectionString;
		tokenizedResponse = new Dictionary<string, object> ();
	}

	public Dictionary<string, object> getResponseAsAToken ()
	{
		return tokenizedResponse;
	}

	public string getValue (string key)
	{
		if (tokenizedResponse != null) {
			if (tokenizedResponse ["resourse"].ToString ().Contains (key)) {
				Dictionary<string,string> temp = new Dictionary<string,string> ();
				JObject obj = JObject.Parse (tokenizedResponse ["resourse"].ToString ());
				foreach (var kvp in obj) {
					temp.Add (kvp.Key, kvp.Value.ToString ());
				}

				return temp [key];
			}

		}        
		return null;
	}

	private async Task<string> getWebRequest (string pURL)
	{
		try {		   
			string rawresp = "";

			HttpWebRequest request = default(HttpWebRequest);
			request = (HttpWebRequest)WebRequest.Create (pURL);
			using (var response = (HttpWebResponse)(await Task<WebResponse>.Factory.FromAsync(request.BeginGetResponse, request.EndGetResponse, null)))
			{
				StreamReader reader = new StreamReader(response.GetResponseStream());
				rawresp = reader.ReadToEnd();
				return rawresp ;
			}


		} catch (Exception ex) {

			//Console.WriteLine (ex.ToString ());
			return "ERROR";

		}
	}

	public void JSONresponseAsToken (string response)
	{
		JObject obj = JObject.Parse (response);
		foreach (var kvp in obj) {
			tokenizedResponse.Add (kvp.Key, kvp.Value);
		}
	}


	public async Task<string> InsertTicket (string device_id, string device_type, string ticket_id, string ticket_subject, string ticket_detail, string company, string ticket_email, string ticket_phone, string lat, string lon, string time, string afterhour = "No")
	{

		string url;
	
		url = URLConnectionString + Convert.ToString("/ticket?api_method=insertticket&device_id=") + device_id + Convert.ToString("&device_type=") + device_type + Convert.ToString("&ticket_id=") + ticket_id + Convert.ToString("&ticket_subject=") + ticket_subject + Convert.ToString("&ticket_detail=") + ticket_detail + Convert.ToString("&company=") + company + Convert.ToString("&ticket_email=") + ticket_email + Convert.ToString("&ticket_phone=") + ticket_phone + Convert.ToString("&lat=") + lat + Convert.ToString("&lon=") + lon + Convert.ToString("&time=") + time + Convert.ToString("&AfterHour=") + afterhour;
		//url = URLConnectionString + Convert.ToString ("/ticket?api_method=insertticket&device_id=") + device_id + Convert.ToString ("&device_type=") + device_type + Convert.ToString ("&ticket_id=") + ticket_id + Convert.ToString ("&ticket_subject=") + ticket_subject + Convert.ToString ("&ticket_detail=") + ticket_detail + Convert.ToString ("&company=") + company + Convert.ToString ("&ticket_email=") + ticket_email + Convert.ToString ("&ticket_phone=") + ticket_phone + Convert.ToString ("&lat=") + lat + Convert.ToString ("&lon=") + lon + Convert.ToString ("&time=") + time;

		Debug.WriteLine (url);

	

		return await getWebRequest (url);
	}

	public async Task<string> InsertUser_Details (string user_name, string user_device_id, string user_company, string user_email, string user_phone)
	{

		string url;
	
		url = URLConnectionString + Convert.ToString ("/ticket?api_method=insertuser_details&user_name=") + user_name + Convert.ToString ("&user_device_id=") + user_device_id + Convert.ToString ("&user_company=") + user_company + Convert.ToString ("&user_email=") + user_email + Convert.ToString ("&user_phone=") + user_phone ;
		Debug.WriteLine (url);

		return await getWebRequest (url);
	}

	public async Task<string> GetGeolocation ()
	{

		string url;
		url = URLConnectionString + Convert.ToString ("/ticket?api_method=getgeolocation");

		return await getWebRequest (url);
	}

	public class Contact
	{
		public string ContactSMS { get; set; }
		public string ContactPhone { get; set; }
	}

	public async Task<List<Contact>> GetDynamicsContact (string email, string timeoffday, string gpslong, string gpslat)
	{
		try
		{
			string rawresp = "";
			Dictionary<string,object> rd = new Dictionary<string,object> ();

			string url;
			url = URLConnectionString + Convert.ToString ("/ticket?api_method=getphone_number&submitteremail=")+email+
				Convert.ToString("&timeoffday=")+timeoffday+
				Convert.ToString("&gpslong=")+gpslong+
				Convert.ToString("&gpslat=")+gpslat;
			rawresp = await getWebRequest (url);
			Debug.WriteLine(url);
			Debug.WriteLine(rawresp);
			List<Contact > tList = new List<Contact >();
			JObject obj1 = JObject.Parse (rawresp);
			foreach (var contact in obj1["resourse"]){
				tList.Add( JsonConvert.DeserializeObject<Contact>(contact.ToString()));
			}
			return tList;
		}
		catch 
		{
			return null;
		}
	}

	public class Message
	{
		public string ContactMessage { get; set; }
	}

	public async Task<List<Message>> GetDynamicsMessage (string email, string timeoffday, string gpslong, string gpslat)
	{
		try
		{
			string rawresp = "";
			Dictionary<string,object> rd = new Dictionary<string,object> ();

			string url;
			url = URLConnectionString + Convert.ToString ("/ticket?api_method=getmessage&submitteremail=")+email+
				Convert.ToString("&timeoffday=")+timeoffday+
				Convert.ToString("&gpslong=")+gpslong+
				Convert.ToString("&gpslat=")+gpslat;
			rawresp = await getWebRequest (url);

			List<Message > tList = new List<Message >();
			JObject obj1 = JObject.Parse (rawresp);
			foreach (var message in obj1["resourse"]){
				tList.Add( JsonConvert.DeserializeObject<Message>(message.ToString()));
			}
			return tList;
		}
		catch 
		{
			return null;
		}
	}

	public class TicketHistory
	{
		public long Id { get; set; }
		public string Ticket_subject { get; set; }
		public string Ticket_detail { get; set; }
	}

	public async Task<List<TicketHistory>> GetTicketHistory (string email)
	{
		try
		{
			string rawresp = "";
			Dictionary<string,object> rd = new Dictionary<string,object> ();

			string url;
			url = URLConnectionString + Convert.ToString ("/ticket?api_method=gethistory&submitteremail=") + email;
			rawresp = await getWebRequest (url);

			List<TicketHistory > tList = new List<TicketHistory >();
			JObject obj1 = JObject.Parse (rawresp);
			foreach (var tickethistory in obj1["resourse"]){
				tList.Add( JsonConvert.DeserializeObject<TicketHistory>(tickethistory.ToString()));
			}
			return tList;
		}
		catch 
		{
			return null;
		}
	}
	public class Customer{
		public string user_company{ get; set;}
		public string user_email{ get; set;}
		public string user_name{ get; set;}
		public string user_phone{ get; set;}
		public string user_login{ get; set;}
		public string user_password{ get; set;}
		public string customer_id{ get; set;}
		public string user_device_id{ get; set;}
		public string id{ get; set;}
	}

	public async Task<List<Customer>> GetCustomer(string email, string user_device_id)
	{
		try {	

			string rawresp = "";
			Dictionary<string,object> rd = new Dictionary<string,object> ();

			string url;
			url = URLConnectionString + Convert.ToString ("/ticket?api_method=getcustomer_id&user_email=") + email + Convert.ToString ("&user_device_id=") + user_device_id;
			rawresp = await getWebRequest (url);

			List<Customer> tList = new List<Customer> ();
			JObject obj1 = JObject.Parse (rawresp);


			foreach (var customer in obj1["resourse"]) {
				tList.Add (JsonConvert.DeserializeObject<Customer> (customer.ToString ()));
			}

			return tList;
		} catch {
			return null;
		}



	}


}


