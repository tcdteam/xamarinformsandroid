// Helpers/Settings.cs
using Refractored.Xam.Settings;
using Refractored.Xam.Settings.Abstractions;

namespace TCDButtonCrossPlatform.Helpers
{
  /// <summary>
  /// This is the Settings static class that can be used in your Core solution or in any
  /// of your client applications. All settings are laid out the same exact way with getters
  /// and setters. 
  /// </summary>
  public static class Settings
  {
    private static ISettings AppSettings
    {
      get
      {
        return CrossSettings.Current;
      }
    }

    #region Setting Constants

    private const string SettingsKey = "settings_key";
    private static readonly string SettingsDefault = string.Empty;
	
		// Server
	private const string ServerKey = "server_key";
	private static readonly string ServerDefault = "125.255.133.224:8081";

	private const string UserEmailKey = "user_email_key";
	private static readonly string UserEmailDefault = string.Empty;

	private const string UserNameKey = "user_name_key";
	private static readonly string UserNameDefault = string.Empty;

	private const string UserSaveKey = "user_save_key";
	private static readonly string UserSaveDefault = "false";

	private const string UserCompanyKey = "user_company_key";
	private static readonly string UserCompanyDefault = string.Empty;

	private const string UserPhoneKey = "user_phone_key";
	private static readonly string UserPhoneDefault = string.Empty;

		private const string LatitudeKey = "latitude_key";
		private static readonly string LatitudeDefault = string.Empty;

		private const string LongitudeKey = "longitude_key";
		private static readonly string LongitudeDefault = string.Empty;

	#endregion

    public static string GeneralSettings
    {
      get
      {
        return AppSettings.GetValueOrDefault(SettingsKey, SettingsDefault);
      }
      set
      {
        AppSettings.AddOrUpdateValue(SettingsKey, value);
      }
    }

	public static string Server
	{
		get
		{
			return AppSettings.GetValueOrDefault(ServerKey, ServerDefault);
		}
		set
		{
			AppSettings.AddOrUpdateValue(ServerKey, value);
		}
	}
	
	public static string UserEmail
	{
		get
		{
			return AppSettings.GetValueOrDefault (UserEmailKey, UserEmailDefault);
		}
		set
		{ 
			AppSettings.AddOrUpdateValue (UserEmailKey, value);
		}
		
	}

	public static string UserName
	{
		get
		{
			return AppSettings.GetValueOrDefault (UserNameKey, UserNameDefault);
		}
		set
		{ 
			AppSettings.AddOrUpdateValue (UserNameKey, value);
		}

	}

	public static string UserSave
	{
		get
		{
			return AppSettings.GetValueOrDefault (UserSaveKey, UserSaveDefault);
		}
		set
		{ 
			AppSettings.AddOrUpdateValue (UserSaveKey, value);
		}

	}
	public static string UserCompany
	{
		get
		{
			return AppSettings.GetValueOrDefault (UserCompanyKey, UserCompanyDefault);
		}
		set
		{ 
			AppSettings.AddOrUpdateValue (UserCompanyKey, value);
		}

	}
	public static string UserPhone
	{
		get
		{
			return AppSettings.GetValueOrDefault (UserPhoneKey, UserPhoneDefault);
		}
		set
		{ 
			AppSettings.AddOrUpdateValue (UserPhoneKey, value);
		}

	}
	public static string Latitude
		{
			get
			{
				return AppSettings.GetValueOrDefault (LatitudeKey, LatitudeDefault);
			}
			set
			{ 
				AppSettings.AddOrUpdateValue (LatitudeKey, value);
			}

		}
	public static string Longitude
		{
			get
			{
				return AppSettings.GetValueOrDefault (LongitudeKey, LongitudeDefault);
			}
			set
			{ 
				AppSettings.AddOrUpdateValue (LongitudeKey, value);
			}

		}
  }
}