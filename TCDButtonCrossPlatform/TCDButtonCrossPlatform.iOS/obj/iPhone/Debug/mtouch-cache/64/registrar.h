#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#pragma clang diagnostic ignored "-Wtypedef-redefinition"
#pragma clang diagnostic ignored "-Wobjc-designated-initializers"
#define DEBUG 1
#include <stdarg.h>
#include <xamarin/xamarin.h>
#include <objc/objc.h>
#include <objc/runtime.h>
#include <objc/message.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>
#import <CoreImage/CoreImage.h>
#import <Photos/Photos.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <GameKit/GameKit.h>
#import <QuartzCore/QuartzCore.h>
#import <QuartzCore/CAEmitterBehavior.h>
#import <AVKit/AVKit.h>
#import <UserNotifications/UserNotifications.h>
#import <WatchKit/WatchKit.h>
#import <WebKit/WebKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <PushKit/PushKit.h>
#import <SceneKit/SceneKit.h>
#import <Accounts/Accounts.h>
#import <SpriteKit/SpriteKit.h>
#import <ReplayKit/ReplayKit.h>
#import <AddressBookUI/AddressBookUI.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AdSupport/AdSupport.h>
#import <GameplayKit/GameplayKit.h>
#import <SafariServices/SafariServices.h>
#import <CoreMIDI/CoreMIDI.h>
#import <CoreMotion/CoreMotion.h>
#import <AudioUnit/AudioUnit.h>
#import <CloudKit/CloudKit.h>
#import <Contacts/Contacts.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import <CoreData/CoreData.h>
#import <Messages/Messages.h>
#import <CallKit/CallKit.h>
#import <Metal/Metal.h>
#import <CoreSpotlight/CoreSpotlight.h>
#import <EventKit/EventKit.h>
#import <GLKit/GLKit.h>
#import <GameController/GameController.h>
#import <HealthKit/HealthKit.h>
#import <HomeKit/HomeKit.h>
#import <Intents/Intents.h>
#import <JavaScriptCore/JavaScriptCore.h>
#import <MessageUI/MessageUI.h>
#import <MetalKit/MetalKit.h>
#import <MetalPerformanceShaders/MetalPerformanceShaders.h>
#import <ModelIO/ModelIO.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>
#import <NetworkExtension/NetworkExtension.h>
#import <CoreTelephony/CoreTelephonyDefines.h>
#import <CoreTelephony/CTCall.h>
#import <CoreTelephony/CTCallCenter.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTSubscriber.h>
#import <CoreTelephony/CTSubscriberInfo.h>
#import <Twitter/Twitter.h>
#import <Social/Social.h>
#import <StoreKit/StoreKit.h>
#import <ExternalAccessory/ExternalAccessory.h>
#import <ContactsUI/ContactsUI.h>
#import <Speech/Speech.h>
#import <CoreAudioKit/CoreAudioKit.h>
#import <NotificationCenter/NotificationCenter.h>
#import <PassKit/PassKit.h>
#import <NewsstandKit/NewsstandKit.h>
#import <VideoSubscriberAccount/VideoSubscriberAccount.h>
#import <WatchConnectivity/WatchConnectivity.h>
#import <PhotosUI/PhotosUI.h>
#import <QuickLook/QuickLook.h>
#import <EventKitUI/EventKitUI.h>
#import <HealthKitUI/HealthKitUI.h>
#import <LocalAuthentication/LocalAuthentication.h>
#import <iAd/iAd.h>
#import <CoreGraphics/CoreGraphics.h>

@class CoreImage_CILinearToSRGBToneCurve;
@class CoreImage_CIBlendFilter;
@class CoreImage_CILuminosityBlendMode;
@class CoreImage_CIMaskToAlpha;
@class CoreImage_CIMaskedVariableBlur;
@class CoreImage_CIMaximumComponent;
@class CoreImage_CICompositingFilter;
@class CoreImage_CIMaximumCompositing;
@class CoreImage_CIMedianFilter;
@class CoreImage_CIMinimumComponent;
@class CoreImage_CIMinimumCompositing;
@class CoreImage_CITransitionFilter;
@class CoreImage_CIModTransition;
@class CoreImage_CIMotionBlur;
@class CoreImage_CIMultiplyBlendMode;
@class CoreImage_CIMultiplyCompositing;
@class CoreImage_CINinePartStretched;
@class CoreImage_CINinePartTiled;
@class CoreImage_CINoiseReduction;
@class CoreImage_CITileFilter;
@class CoreImage_CIOpTile;
@class CoreImage_CIOverlayBlendMode;
@class CoreImage_CIPageCurlTransition;
@class CoreImage_CIPageCurlWithShadowTransition;
@class CoreImage_CIParallelogramTile;
@class CoreImage_CICodeGenerator;
@class CoreImage_CIPdf417BarcodeGenerator;
@class CoreImage_CIPerspectiveTransform;
@class CoreImage_CIPerspectiveCorrection;
@class CoreImage_CIPerspectiveTile;
@class __MonoTouch_UIVideoStatusDispatcher;
@class CoreImage_CIPerspectiveTransformWithExtent;
@class CoreImage_CIPhotoEffect;
@class CoreImage_CIPhotoEffectChrome;
@class CoreImage_CIPhotoEffectFade;
@class CoreImage_CIPhotoEffectInstant;
@class CoreImage_CIPhotoEffectMono;
@class CoreImage_CIPhotoEffectNoir;
@class CoreImage_CIPhotoEffectProcess;
@class CoreImage_CIPhotoEffectTonal;
@class AVFoundation_InternalAVAudioPlayerDelegate;
@class AVFoundation_InternalAVAudioRecorderDelegate;
@class AVFoundation_InternalAVAudioSessionDelegate;
@class CoreImage_CIPhotoEffectTransfer;
@class CoreImage_CIPinLightBlendMode;
@class CoreImage_CIDistortionFilter;
@class CoreImage_CIPinchDistortion;
@class CoreImage_CIPixellate;
@class CoreImage_CIPointillize;
@class CoreImage_CIQRCodeGenerator;
@class CoreImage_CIRadialGradient;
@class CoreImage_CIRandomGenerator;
@class CoreImage_CIRippleTransition;
@class CoreImage_CIRowAverage;
@class CoreImage_CISRGBToneCurveToLinear;
@class CoreImage_CISaturationBlendMode;
@class CoreImage_CIScreenBlendMode;
@class CoreImage_CIScreenFilter;
@class CoreImage_CISepiaTone;
@class CoreImage_CIShadedMaterial;
@class CoreImage_CISharpenLuminance;
@class CoreImage_CISixfoldReflectedTile;
@class CoreImage_CISixfoldRotatedTile;
@class CoreImage_CILinearGradient;
@class CoreImage_CISmoothLinearGradient;
@class CoreImage_CISoftLightBlendMode;
@class CoreImage_CISourceAtopCompositing;
@class CoreImage_CISourceInCompositing;
@class CoreImage_CISourceOutCompositing;
@class CoreImage_CISourceOverCompositing;
@class CoreImage_CISpotColor;
@class CoreImage_CISpotLight;
@class CoreImage_CIStarShineGenerator;
@class CoreImage_CIStraightenFilter;
@class CoreImage_CIStretchCrop;
@class CoreImage_CIStripesGenerator;
@class CoreImage_CISubtractBlendMode;
@class CoreImage_CISunbeamsGenerator;
@class CoreImage_CISwipeTransition;
@class CoreImage_CITemperatureAndTint;
@class CoreImage_CIThermal;
@class CoreImage_CIToneCurve;
@class CoreImage_CITorusLensDistortion;
@class CoreImage_CITriangleKaleidoscope;
@class CoreImage_CITriangleTile;
@class CoreImage_CITwelvefoldReflectedTile;
@class CoreImage_CITwirlDistortion;
@class CoreImage_CIUnsharpMask;
@class Foundation_InternalNSNotificationHandler;
@class CoreImage_CIVibrance;
@class CoreImage_CIVignette;
@class CoreImage_CIVignetteEffect;
@class CoreImage_CIVortexDistortion;
@class CoreImage_CIWhitePointAdjust;
@class CoreImage_CIXRay;
@class CoreImage_CIZoomBlur;
@class GameKit_Mono_GKSessionDelegate;
@class AddressBookUI_InternalABPersonViewControllerDelegate;
@class AddressBookUI_InternalABUnknownPersonViewControllerDelegate;
@class AddressBookUI_InternalABNewPersonViewControllerDelegate;
@class AddressBookUI_InternalABPeoplePickerNavigationControllerDelegate;
@class __MonoMac_NSActionDispatcher;
@class __MonoMac_ActionDispatcher;
@class __Xamarin_NSTimerActionDispatcher;
@class __MonoMac_NSAsyncActionDispatcher;
@protocol UIPickerViewModel;
@class MessageUI_Mono_MFMailComposeViewControllerDelegate;
@class MessageUI_Mono_MFMessageComposeViewControllerDelegate;
@class UIKit_UIControlEventProxy;
@class __MonoTouch_UIImageStatusDispatcher;
@protocol CALayerDelegate;
@class CoreImage_CIAztecCodeGenerator;
@class CoreImage_CIBarsSwipeTransition;
@class CoreImage_CIAccordionFoldTransition;
@class CoreImage_CIBlendWithMask;
@class CoreImage_CIBlendWithAlphaMask;
@class CoreImage_CIAdditionCompositing;
@class CoreImage_CIAffineFilter;
@class CoreImage_CIAffineClamp;
@class CoreImage_CIBloom;
@class CoreImage_CIBoxBlur;
@class CoreImage_CIAffineTile;
@class CoreImage_CIBumpDistortion;
@class CoreImage_CIAffineTransform;
@class CoreImage_CIColorBlendMode;
@class CoreImage_CIBumpDistortionLinear;
@class CoreImage_CIAreaAverage;
@class CoreImage_CIColorBurnBlendMode;
@class CoreImage_CICheckerboardGenerator;
@class CoreImage_CIAreaHistogram;
@class CoreImage_CIColorClamp;
@class CoreImage_CICircleSplashDistortion;
@class CoreImage_CIAreaMaximum;
@class CoreImage_CIColorControls;
@class CoreImage_CICircularScreen;
@class CoreImage_CIAreaMaximumAlpha;
@class CoreImage_CIColorCrossPolynomial;
@class CoreImage_CICircularWrap;
@class CoreImage_CIAreaMinimum;
@class CoreImage_CIColorCube;
@class CoreImage_CIClamp;
@class CoreImage_CIAreaMinimumAlpha;
@class CoreImage_CICmykHalftone;
@class CoreImage_CIColorPosterize;
@class CoreImage_CICode128BarcodeGenerator;
@class CoreImage_CIColumnAverage;
@class CoreImage_CIDarkenBlendMode;
@class CoreImage_CIComicEffect;
@class CoreImage_CIDepthOfField;
@class CoreImage_CIConstantColorGenerator;
@class CoreImage_CIExposureAdjust;
@class CoreImage_CIFaceBalance;
@class CoreImage_CIDifferenceBlendMode;
@class CoreImage_CIColorCubeWithColorSpace;
@class CoreImage_CIDiscBlur;
@class CoreImage_CIColorDodgeBlendMode;
@class CoreImage_CIDisintegrateWithMaskTransition;
@class CoreImage_CIColorInvert;
@class CoreImage_CIDisplacementDistortion;
@class CoreImage_CIDissolveTransition;
@class CoreImage_CIFalseColor;
@class CoreImage_CIColorMap;
@class CoreImage_CIColorMatrix;
@class CoreImage_CIColorMonochrome;
@class CoreImage_CIColorPolynomial;
@class CoreImage_CIGammaAdjust;
@class CoreImage_CIGaussianBlur;
@class CoreImage_CIGaussianGradient;
@class CoreImage_CIGlassDistortion;
@class CoreImage_CIGlassLozenge;
@class CoreImage_CIGlideReflectedTile;
@class CoreImage_CIGloom;
@class CoreImage_CIHardLightBlendMode;
@class CoreImage_CIHatchedScreen;
@class CoreImage_CIHeightFieldFromMask;
@class CoreImage_CIHexagonalPixellate;
@class CoreImage_CIHighlightShadowAdjust;
@class CoreImage_CIHistogramDisplayFilter;
@class CoreImage_CIHoleDistortion;
@class CoreImage_CIHueAdjust;
@class CoreImage_CIHueBlendMode;
@class CoreImage_CIHueSaturationValueGradient;
@class CoreImage_CIConvolutionCore;
@class CoreImage_CIConvolution3X3;
@class CoreImage_CIConvolution5X5;
@class CoreImage_CIConvolution7X7;
@class CoreImage_CIDivideBlendMode;
@class CoreImage_CIConvolution9Horizontal;
@class CoreImage_CIDotScreen;
@class CoreImage_CIConvolution9Vertical;
@class CoreImage_CIDroste;
@class CoreImage_CIEdgeWork;
@class CoreImage_CICopyMachineTransition;
@class CoreImage_CIEdges;
@class CoreImage_CICrop;
@class CoreImage_CIEightfoldReflectedTile;
@class CoreImage_CICrystallize;
@class CoreImage_CIExclusionBlendMode;
@class CoreImage_CIFlashTransition;
@class CoreImage_CIFourfoldReflectedTile;
@class CoreImage_CIFourfoldRotatedTile;
@class CoreImage_CILinearBurnBlendMode;
@class CoreImage_CIFourfoldTranslatedTile;
@class CoreImage_CILinearDodgeBlendMode;
@protocol CIImageProvider;
@class CoreImage_CIKaleidoscope;
@class CoreImage_CILanczosScaleTransform;
@class CoreImage_CILenticularHaloGenerator;
@class CoreImage_CILightTunnel;
@class CoreImage_CILightenBlendMode;
@class CoreImage_CILineOverlay;
@class CoreImage_CILineScreen;
@protocol UIAccessibilityContainer;
@protocol UICollectionViewSource;
@class Xamarin_Forms_Platform_iOS_FormsApplicationDelegate;
@class XLabs_Forms_XFormsApplicationDelegate;
@class AppDelegate;
@class UIKit_UIView_UIViewAppearance;
@class UIKit_UISearchBar_UISearchBarAppearance;
@class UIKit_UISearchBar__UISearchBarDelegate;
@class UIKit_UISearchController___Xamarin_UISearchResultsUpdating;
@class UIKit_UIControl_UIControlAppearance;
@class UIKit_UISegmentedControl_UISegmentedControlAppearance;
@class MapKit_MKAnnotationView_MKAnnotationViewAppearance;
@class MapKit_MKPinAnnotationView_MKPinAnnotationViewAppearance;
@class GameKit_GKMatch__GKMatchDelegate;
@class UIKit_UIScrollView_UIScrollViewAppearance;
@class UIKit_UITableView_UITableViewAppearance;
@class UIKit_UITableViewCell_UITableViewCellAppearance;
@class MapKit_MKOverlayView_MKOverlayViewAppearance;
@class MapKit_MKOverlayPathView_MKOverlayPathViewAppearance;
@class MapKit_MKPolygonView_MKPolygonViewAppearance;
@class UIKit_UITextField__UITextFieldDelegate;
@class UIKit_UITextField_UITextFieldAppearance;
@class UIKit_UIScrollView__UIScrollViewDelegate;
@class UIKit_UITextView__UITextViewDelegate;
@class UIKit_UITextView_UITextViewAppearance;
@class AVFoundation_AVCaptureFileOutput_recordingProxy;
@class UIKit_UIToolbar_UIToolbarAppearance;
@class MapKit_MKPolylineView_MKPolylineViewAppearance;
@class UIKit_UIView__UIViewStaticCallback;
@class GameKit_GKMatchmakerViewController__GKMatchmakerViewControllerDelegate;
@class AVFoundation_AVSpeechSynthesizer__AVSpeechSynthesizerDelegate;
@class UIKit_UIGestureRecognizer__UIGestureRecognizerDelegate;
@class __UIGestureRecognizerToken;
@class __UIGestureRecognizerParameterlessToken;
@class __UIGestureRecognizerParametrizedToken;
@class UIKit_UIBarItem_UIBarItemAppearance;
@class UIKit_UIBarButtonItem_UIBarButtonItemAppearance;
@class MapKit_MKUserTrackingBarButtonItem_MKUserTrackingBarButtonItemAppearance;
@class Foundation_NSKeyedUnarchiver__NSKeyedUnarchiverDelegate;
@class Foundation_NSKeyedArchiver__NSKeyedArchiverDelegate;
@class Foundation_NSNetService__NSNetServiceDelegate;
@class UIKit_UIImagePickerController__UIImagePickerControllerDelegate;
@class Foundation_NSStream__NSStreamDelegate;
@class UIKit_UIImageView_UIImageViewAppearance;
@class MonoTouch_GKSession_ReceivedObject;
@class Photos_PHPhotoLibrary___phlib_observer;
@class MediaPlayer_MPMediaPickerController__MPMediaPickerControllerDelegate;
@class PhotosUI_PHLivePhotoView_PHLivePhotoViewAppearance;
@class QuickLook_QLPreviewController__QLPreviewControllerDelegate;
@class GameKit_GKTurnBasedMatchmakerViewController_GKTurnBasedMatchmakerViewControllerAppearance;
@class UIKit_UIInputView_UIInputViewAppearance;
@class AddressBookUI_ABPeoplePickerNavigationController_ABPeoplePickerNavigationControllerAppearance;
@class UIKit_UILabel_UILabelAppearance;
@class CoreLocation_CLLocationManager__CLLocationManagerDelegate;
@class __UILongPressGestureRecognizer;
@class CoreAnimation_CAAnimation__CAAnimationDelegate;
@class MediaPlayer_MPVolumeView_MPVolumeViewAppearance;
@class MessageUI_MFMailComposeViewController_MFMailComposeViewControllerAppearance;
@class CoreBluetooth_CBCentralManager__CBCentralManagerDelegate;
@class CoreBluetooth_CBPeripheralManager__CBPeripheralManagerDelegate;
@class CoreBluetooth_CBPeripheral__CBPeripheralDelegate;
@class MessageUI_MFMessageComposeViewController_MFMessageComposeViewControllerAppearance;
@class __NSObject_Disposer;
@class __XamarinObjectObserver;
@class UIKit_UINavigationBar_UINavigationBarAppearance;
@class Messages_MSStickerBrowserView_MSStickerBrowserViewAppearance;
@class Messages_MSStickerView_MSStickerViewAppearance;
@class UIKit_UIPageControl_UIPageControlAppearance;
@class UIKit_UIPageViewController__UIPageViewControllerDelegate;
@class UIKit_UIPageViewController__UIPageViewControllerDataSource;
@class __UIPanGestureRecognizer;
@class UIKit_UIPickerView_UIPickerViewAppearance;
@class HomeKit_HMHome__HMHomeDelegate;
@class __UIPinchGestureRecognizer;
@class UIKit_UIPopoverBackgroundView_UIPopoverBackgroundViewAppearance;
@class UIKit_UIPopoverController__UIPopoverControllerDelegate;
@class UIKit_UIPopoverPresentationController__UIPopoverPresentationControllerDelegate;
@class __MonoMac_FuncBoolDispatcher;
@class UIKit_UIActionSheet__UIActionSheetDelegate;
@class UIKit_UIActionSheet_UIActionSheetAppearance;
@class UIKit_UIAlertView__UIAlertViewDelegate;
@class UIKit_UIAlertView_UIAlertViewAppearance;
@class UIKit_UIBarButtonItem_Callback;
@class UIKit_UIButton_UIButtonAppearance;
@class UIKit_UIDocumentMenuViewController__UIDocumentMenuDelegate;
@class UIKit_UIDocumentPickerViewController__UIDocumentPickerDelegate;
@class UIKit_UICollectionView_UICollectionViewAppearance;
@class UIKit_UIPreviewInteraction__UIPreviewInteractionDelegate;
@class __UIRotationGestureRecognizer;
@class __UITapGestureRecognizer;
@class __UISwipeGestureRecognizer;
@class __UIScreenEdgePanGestureRecognizer;
@class UIKit_UIPrintInteractionController__UIPrintInteractionControllerDelegate;
@class UIKit_UIProgressView_UIProgressViewAppearance;
@class EventKitUI_EKCalendarChooser__EKCalendarChooserDelegate;
@class UIKit_UIRefreshControl_UIRefreshControlAppearance;
@class EventKitUI_EKEventEditViewController__EKEventEditViewDelegate;
@class EventKitUI_EKEventEditViewController_EKEventEditViewControllerAppearance;
@class EventKitUI_EKEventViewController__EKEventViewDelegate;
@class ExternalAccessory_EAAccessory__EAAccessoryDelegate;
@class ExternalAccessory_EAWiFiUnconfiguredAccessoryBrowser__EAWiFiUnconfiguredAccessoryBrowserDelegate;
@class Foundation_NSCache__NSCacheDelegate;
@class SceneKit_SCNPhysicsWorld__SCNPhysicsContactDelegate;
@class HealthKitUI_HKActivityRingView_HKActivityRingViewAppearance;
@class UIKit_UISlider_UISliderAppearance;
@class HomeKit_HMAccessory__HMAccessoryDelegate;
@class HomeKit_HMAccessoryBrowser__HMAccessoryBrowserDelegate;
@class UIKit_UISplitViewController__UISplitViewControllerDelegate;
@class UIKit_UIStackView_UIStackViewAppearance;
@class HomeKit_HMCameraView_HMCameraViewAppearance;
@class UIKit_UIStepper_UIStepperAppearance;
@class MetalKit_MTKView_MTKViewAppearance;
@class UIKit_UISwitch_UISwitchAppearance;
@class UIKit_UITabBar__UITabBarDelegate;
@class UIKit_UITabBar_UITabBarAppearance;
@class SceneKit_SCNView_SCNViewAppearance;
@class UIKit_UITabBarController__UITabBarControllerDelegate;
@class HomeKit_HMHomeManager__HMHomeManagerDelegate;
@class UIKit_UITabBarItem_UITabBarItemAppearance;
@class CoreAudioKit_CAInterAppAudioTransportView_CAInterAppAudioTransportViewAppearance;
@class Foundation_NSMetadataQuery__NSMetadataQueryDelegate;
@class UIKit_UITableViewHeaderFooterView_UITableViewHeaderFooterViewAppearance;
@class CoreAudioKit_CAInterAppAudioSwitcherView_CAInterAppAudioSwitcherViewAppearance;
@class Foundation_NSNetServiceBrowser__NSNetServiceBrowserDelegate;
@class SpriteKit_SKPhysicsWorld__SKPhysicsContactDelegate;
@class GameKit_GKGameCenterViewController__GKGameCenterControllerDelegate;
@class GameKit_GKAchievementViewController__GKAchievementViewControllerDelegate;
@class GameKit_GKAchievementViewController_GKAchievementViewControllerAppearance;
@class GLKit_GLKView__GLKViewDelegate;
@class GLKit_GLKView_GLKViewAppearance;
@class GameKit_GKFriendRequestComposeViewController__GKFriendRequestComposeViewControllerDelegate;
@class GameKit_GKFriendRequestComposeViewController_GKFriendRequestComposeViewControllerAppearance;
@class GameKit_GKChallengeEventHandler__GKChallengeEventHandlerDelegate;
@class UIKit_UIVideoEditorController__UIVideoEditorControllerDelegate;
@class GameKit_GKLeaderboardViewController__GKLeaderboardViewControllerDelegate;
@class GameKit_GKLeaderboardViewController_GKLeaderboardViewControllerAppearance;
@class SpriteKit_SKView_SKViewAppearance;
@class MapKit_MKCircleView_MKCircleViewAppearance;
@class PassKit_PKAddPassButton_PKAddPassButtonAppearance;
@class PassKit_PKAddPassesViewController__PKAddPassesViewControllerDelegate;
@class PassKit_PKPaymentAuthorizationViewController__PKPaymentAuthorizationViewControllerDelegate;
@class StoreKit_SKRequest__SKRequestDelegate;
@class StoreKit_SKProductsRequest__SKProductsRequestDelegate;
@class PassKit_PKPaymentButton_PKPaymentButtonAppearance;
@class StoreKit_SKStoreProductViewController__SKStoreProductViewControllerDelegate;
@class UIKit_NSTextStorage__NSTextStorageDelegate;
@class UIKit_UIActivityIndicatorView_UIActivityIndicatorViewAppearance;
@class UIKit_UICollectionReusableView_UICollectionReusableViewAppearance;
@class UIKit_UIAccelerometer__UIAccelerometerDelegate;
@class MapKit_MKMapView__MKMapViewDelegate;
@class MapKit_MKMapView_MKMapViewAppearance;
@class UIKit_UICollisionBehavior__UICollisionBehaviorDelegate;
@class UIKit_UIDatePicker_UIDatePickerAppearance;
@class UIKit_UIVisualEffectView_UIVisualEffectViewAppearance;
@class UIKit_UICollectionViewCell_UICollectionViewCellAppearance;
@class UIKit_UIWebView__UIWebViewDelegate;
@class UIKit_UIWebView_UIWebViewAppearance;
@class UIKit_UIWindow_UIWindowAppearance;
@class UIKit_UIDocumentInteractionController__UIDocumentInteractionControllerDelegate;
@class iAd_ADBannerView__ADBannerViewDelegate;
@class iAd_ADBannerView_ADBannerViewAppearance;
@class WebKit_WKWebView_WKWebViewAppearance;
@class iAd_ADInterstitialAd__ADInterstitialAdDelegate;
@class Xamarin_Forms_Platform_iOS_VisualElementRenderer_1;
@class Xamarin_Forms_Platform_iOS_ViewRenderer_2;
@class Xamarin_Forms_Platform_iOS_ViewRenderer;
@class Xamarin_Forms_Maps_iOS_MapRenderer;
@class Xamarin_Forms_Maps_iOS_MapDelegate;
@class Xamarin_Forms_Platform_iOS_iOS7ButtonContainer;
@class Xamarin_Forms_Platform_iOS_GlobalCloseContextGestureRecognizer;
@class Xamarin_Forms_Platform_iOS_ModalWrapper;
@class Xamarin_Forms_Platform_iOS_PlatformRenderer;
@class Xamarin_Forms_Platform_iOS_CellTableViewCell;
@class Xamarin_Forms_Platform_iOS_ActivityIndicatorRenderer;
@class Xamarin_Forms_Platform_iOS_BoxRenderer;
@class Xamarin_Forms_Platform_iOS_NoCaretField;
@class Xamarin_Forms_Platform_iOS_EditorRenderer;
@class Xamarin_Forms_Platform_iOS_EntryRenderer;
@class Xamarin_Forms_Platform_iOS_FrameRenderer;
@class Xamarin_Forms_Platform_iOS_LabelRenderer;
@class Xamarin_Forms_Platform_iOS_ProgressBarRenderer;
@class Xamarin_Forms_Platform_iOS_ScrollViewRenderer;
@class Xamarin_Forms_Platform_iOS_SearchBarRenderer;
@class Xamarin_Forms_Platform_iOS_SliderRenderer;
@class Xamarin_Forms_Platform_iOS_StepperRenderer;
@class Xamarin_Forms_Platform_iOS_SwitchRenderer;
@class Xamarin_Forms_Platform_iOS_TabbedRenderer;
@class Xamarin_Forms_Platform_iOS_TableViewModelRenderer;
@class Xamarin_Forms_Platform_iOS_UnEvenTableViewModelRenderer;
@class Xamarin_Forms_Platform_iOS_TableViewRenderer;
@class Xamarin_Forms_Platform_iOS_ChildViewController;
@class Xamarin_Forms_Platform_iOS_EventedViewController;
@class Xamarin_Forms_Platform_iOS_ToolbarRenderer;
@class Xamarin_Forms_Platform_iOS_ContextActionsCell_SelectGestureRecognizer;
@class Xamarin_Forms_Platform_iOS_ContextActionsCell_MoreActionSheetController;
@class Xamarin_Forms_Platform_iOS_ContextActionsCell_MoreActionSheetDelegate;
@class Xamarin_Forms_Platform_iOS_ContextActionsCell;
@class Xamarin_Forms_Platform_iOS_ContextScrollViewDelegate;
@class Xamarin_Forms_Platform_iOS_RendererFactory_DefaultRenderer;
@class Xamarin_Forms_Platform_iOS_EntryCellRenderer_EntryCellTableViewCell;
@class Xamarin_Forms_Platform_iOS_ViewCellRenderer_ViewTableCell;
@class Xamarin_Forms_Platform_iOS_ButtonRenderer;
@class Xamarin_Forms_Platform_iOS_CarouselPageRenderer_PageContainer;
@class Xamarin_Forms_Platform_iOS_CarouselPageRenderer;
@class Xamarin_Forms_Platform_iOS_DatePickerRenderer;
@class Xamarin_Forms_Platform_iOS_ImageRenderer;
@class Xamarin_Forms_Platform_iOS_ListViewRenderer_ListViewDataSource;
@class Xamarin_Forms_Platform_iOS_ListViewRenderer_UnevenListViewDataSource;
@class Xamarin_Forms_Platform_iOS_ListViewRenderer;
@class Xamarin_Forms_Platform_iOS_NavigationMenuRenderer_NavigationCell;
@class Xamarin_Forms_Platform_iOS_NavigationMenuRenderer;
@class Xamarin_Forms_Platform_iOS_NavigationRenderer_SecondaryToolbar;
@class Xamarin_Forms_Platform_iOS_NavigationRenderer_ParentingViewController;
@class Xamarin_Forms_Platform_iOS_NavigationRenderer;
@class Xamarin_Forms_Platform_iOS_OpenGLViewRenderer_Delegate;
@class Xamarin_Forms_Platform_iOS_OpenGLViewRenderer;
@class Xamarin_Forms_Platform_iOS_PageRenderer;
@class Xamarin_Forms_Platform_iOS_PhoneMasterDetailRenderer_ChildViewController;
@class Xamarin_Forms_Platform_iOS_PhoneMasterDetailRenderer;
@class Xamarin_Forms_Platform_iOS_PickerRenderer_PickerSource;
@class Xamarin_Forms_Platform_iOS_PickerRenderer;
@class Xamarin_Forms_Platform_iOS_TabletMasterDetailRenderer_InnerDelegate;
@class Xamarin_Forms_Platform_iOS_TabletMasterDetailRenderer;
@class Xamarin_Forms_Platform_iOS_TimePickerRenderer;
@class Xamarin_Forms_Platform_iOS_WebViewRenderer_CustomWebViewDelegate;
@class Xamarin_Forms_Platform_iOS_WebViewRenderer;
@class Xamarin_Forms_Platform_iOS_ToolbarItemExtensions_PrimaryToolbarItem;
@class Xamarin_Forms_Platform_iOS_ToolbarItemExtensions_SecondaryToolbarItem_SecondaryToolbarItemContent;
@class Xamarin_Forms_Platform_iOS_ToolbarItemExtensions_SecondaryToolbarItem;
@class Xamarin_Forms_Platform_iOS_NavigationMenuRenderer_DataSource;
@class System_Net_Http_NSUrlSessionHandler_DataTaskDelegate;
@class Plugin_Geolocator_GeolocationSingleUpdateDelegate;
@class XLabs_Forms_Controls_NoCaretField;
@class XLabs_Forms_Controls_CalendarDayView;
@class XLabs_Forms_Controls_MonthGridView;
@class XLabs_Forms_Controls_CameraViewRenderer;
@class CameraPreview;
@class XLabs_Forms_Controls_CheckBoxRenderer;
@class CheckBoxView;
@class XLabs_Forms_Controls_CircleImageRenderer;
@class XLabs_Forms_Controls_ExtendedLabelRenderer;
@class XLabs_Forms_Controls_ExtendedScrollViewRenderer;
@class XLabs_Forms_Controls_ExtendedSwitchRenderer;
@class XLabs_Forms_Controls_ExtendedTableViewRenderer;
@class XLabs_Forms_Controls_GesturesContentViewRenderer;
@class XLabs_Forms_Controls_GridCollectionView;
@class XLabs_Forms_Controls_GridViewCell;
@class XLabs_Forms_Controls_GridItemSelectedViewOverlay;
@class XLabs_Forms_Controls_GridViewRenderer;
@class XLabs_Forms_Controls_HyperLinkLabelRenderer;
@class XLabs_Forms_Controls_IconButtonRenderer;
@class XLabs_Forms_Controls_ImageGalleryRenderer;
@class XLabs_Forms_Controls_RadioButtonRenderer;
@class RadioButtonView;
@class XLabs_Forms_Controls_SegmentedControlViewRenderer;
@class XLabs_Forms_Controls_SensorBarViewRenderer;
@class SensorBarView;
@class XLabs_Forms_Controls_SeparatorRenderer;
@class XLabs_Forms_Controls_UISeparator;
@class XLabs_Forms_Controls_WebImageRenderer;
@class XLabs_Forms_Pages_ExtendedPhoneMasterDetailPageRenderer;
@class XLabs_Forms_Controls_ExtendedButtonRenderer;
@class XLabs_Forms_Controls_ImageButtonRenderer;
@class XLabs_Forms_Controls_HybridWebViewRenderer;
@class XLabs_Forms_Controls_BindablePickerRenderer_PickerSource;
@class XLabs_Forms_Controls_BindablePickerRenderer;
@class XLabs_Forms_Controls_CalendarArrowView;
@class XLabs_Forms_Controls_CalendarMonthView;
@class XLabs_Forms_Controls_CalendarViewRenderer;
@class XLabs_Forms_Controls_DragContentViewRenderer;
@class XLabs_Forms_Controls_DynamicUITableViewRenderer_1;
@class XLabs_Forms_Controls_EditableListViewRenderer_1_EditableListViewSource;
@class XLabs_Forms_Controls_EditableListViewRenderer_1;
@class XLabs_Forms_Controls_ExtendedDatePickerRenderer;
@class XLabs_Forms_Controls_ExtendedEditorRenderer;
@class XLabs_Forms_Controls_ExtendedEntryRenderer;
@class XLabs_Forms_Controls_ExtendedTabbedPageRenderer;
@class XLabs_Forms_Controls_ExtendedTimePickerRenderer;
@class XLabs_Forms_Controls_GridDataSource;
@class XLabs_Forms_Controls_GridViewDelegate;
@class XLabs_Forms_Controls_ImageGalleryView;
@class XLabs_Forms_Pages_ExtendedTabletMasterDetailPageRenderer_ExtendedDelegate;
@class XLabs_Forms_Pages_ExtendedTabletMasterDetailPageRenderer;
@class XLabs_Forms_Controls_DynamicUITableViewRenderer_1_TableDataSource;
@class XLabs_Forms_Controls_DynamicUITableViewRenderer_1_TableViewDelegate;
@class XLabs_Platform_Services_Media_MediaPickerController;
@class XLabs_Platform_Services_Media_MediaPickerPopoverDelegate;
@class XLabs_Platform_Device_BluetoothDevice_BtDelegate;
@class XLabs_Platform_Device_BluetoothHub;
@class XLabs_Platform_Services_Geolocation_GeolocationSingleUpdateDelegate;
@class XLabs_Platform_Services_Media_MediaPickerDelegate;

