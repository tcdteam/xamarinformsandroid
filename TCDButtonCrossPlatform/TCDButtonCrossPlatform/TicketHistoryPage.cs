﻿using System;
using System.Linq;

using Xamarin.Forms;
using System.Collections.Generic;
using System.Diagnostics;

namespace TCDButtonCrossPlatform
{
	public class TicketHistoryPage : ContentPage
	{
		JsonStuff ws;
		List<JsonStuff.TicketHistory> list;
		List<JsonStuff.TicketHistory> listTemp; 
		ListView listView ;
		public  TicketHistoryPage ()
		{
			BackgroundColor = Device.OnPlatform (Color.FromHex("333333"), Color.Default, Color.Default);
			string url = "http://"+TCDButtonCrossPlatform.Helpers.Settings.Server;
			 ws = new JsonStuff (url);

		}
			

		void initializeListView(List<JsonStuff.TicketHistory> list){

			SearchBar searchBar = new SearchBar
			{
				Placeholder = "Ticket",
			};

			listView = new ListView
			{
				//RowHeight = 160
				HasUnevenRows =true
			};

			listView.ItemsSource = list;
			listView.ItemTemplate = new DataTemplate (typeof(TicketHistoryCell));

			this.Title = "Ticket History";
			this.Content = new StackLayout { 
				Children = {
					searchBar,
					listView
				}
			};

			searchBar.TextChanged += (sender, e) => {
//				listTemp = from listsearch in list  
//						where 
//					(listsearch.Ticket_subject.ToLower().Contains(e.NewTextValue) || listsearch.Ticket_detail.ToLower().Contains(e.NewTextValue) || listsearch.Ticket_subject.ToLower().Contains(e.NewTextValue) ) select new{};

				listTemp = list.Where(listsearch=>(listsearch.Id.ToString().Contains(e.NewTextValue) || listsearch.Ticket_detail.ToLower().Contains(e.NewTextValue) || listsearch.Ticket_subject.ToLower().Contains(e.NewTextValue) )).ToList();
				listView.ItemsSource = listTemp;
				listView.ItemTemplate = new DataTemplate (typeof(TicketHistoryCell));
			};
		}

		protected override async void OnAppearing()
		{
			string email = TCDButtonCrossPlatform.Helpers.Settings.UserEmail;
			if (!email.Equals (string.Empty)) {
				list = await ws.GetTicketHistory (email);
				initializeListView (list);
				listView.ItemSelected += OnSelection;
			} else {
				await DisplayAlert("Alert","You must send ticket once or save your email first!","OK");
			}

			base.OnAppearing();
		}
		void OnSelection (object sender, SelectedItemChangedEventArgs e)
		{
			if (e.SelectedItem == null) {
				return; //ItemSelected is called on deselection, which results in SelectedItem being set to null
			}
			//DisplayAlert ("Item Selected", e.SelectedItem.ToString (), "Ok");
			//((ListView)sender).SelectedItem = null; //uncomment line if you want to disable the visual selection state.
			
		}
	}
}


