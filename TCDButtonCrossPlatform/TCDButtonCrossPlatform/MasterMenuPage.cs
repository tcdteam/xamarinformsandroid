﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;


namespace TCDButtonCrossPlatform.Views
{
    public class MasterMenuPage : MasterDetailPage
    {
        MenuPage menuPage;
        public MasterMenuPage()
        {
            menuPage = new MenuPage();

            menuPage.Menu.ItemSelected += (sender, e) => NavigateTo(e.SelectedItem as MenuItem);
			 
            Master = menuPage;
			if (TCDButtonCrossPlatform.Helpers.Settings.Server == "") {
				Detail = new NavigationPage(new ConnectionPage()){BarBackgroundColor=Color.FromHex("333333")};
			} else {
				Detail = new NavigationPage(new FormPage()){BarBackgroundColor=Color.FromHex("333333")};
			}


        }

        void NavigateTo(MenuItem menu)
        {
            if (menu == null)
                return;

            Page displayPage = (Page)Activator.CreateInstance(menu.TargetType);

			Detail = new NavigationPage(displayPage){BarBackgroundColor=Color.FromHex("333333")};

            menuPage.Menu.SelectedItem = null;
            IsPresented = false;
        }
    }
}
