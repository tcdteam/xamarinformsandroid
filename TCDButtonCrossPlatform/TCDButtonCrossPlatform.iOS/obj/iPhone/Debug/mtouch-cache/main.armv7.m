#include "xamarin/xamarin.h"

extern void *mono_aot_module_TCDButtonCrossPlatformiOS_info;
extern void *mono_aot_module_mscorlib_info;
extern void *mono_aot_module_System_Core_info;
extern void *mono_aot_module_System_info;
extern void *mono_aot_module_System_Xml_info;
extern void *mono_aot_module_Mono_Dynamic_Interpreter_info;
extern void *mono_aot_module_Xamarin_iOS_info;
extern void *mono_aot_module_Xamarin_Forms_Maps_iOS_info;
extern void *mono_aot_module_Xamarin_Forms_Platform_iOS_info;
extern void *mono_aot_module_Xamarin_Forms_Core_info;
extern void *mono_aot_module_System_Runtime_info;
extern void *mono_aot_module_System_ComponentModel_Composition_info;
extern void *mono_aot_module_System_ObjectModel_info;
extern void *mono_aot_module_System_IO_info;
extern void *mono_aot_module_System_Collections_info;
extern void *mono_aot_module_System_Threading_Tasks_info;
extern void *mono_aot_module_System_Globalization_info;
extern void *mono_aot_module_System_ComponentModel_info;
extern void *mono_aot_module_System_Linq_Expressions_info;
extern void *mono_aot_module_System_Xml_ReaderWriter_info;
extern void *mono_aot_module_System_Reflection_info;
extern void *mono_aot_module_System_Dynamic_Runtime_info;
extern void *mono_aot_module_System_Threading_info;
extern void *mono_aot_module_System_Runtime_Extensions_info;
extern void *mono_aot_module_System_Linq_info;
extern void *mono_aot_module_System_Reflection_Extensions_info;
extern void *mono_aot_module_System_Diagnostics_Debug_info;
extern void *mono_aot_module_Xamarin_Forms_Platform_info;
extern void *mono_aot_module_System_Net_Http_info;
extern void *mono_aot_module_System_Runtime_Serialization_info;
extern void *mono_aot_module_System_ServiceModel_Internals_info;
extern void *mono_aot_module_Xamarin_Forms_Maps_info;
extern void *mono_aot_module_System_Resources_ResourceManager_info;
extern void *mono_aot_module_TCDButtonCrossPlatform_info;
extern void *mono_aot_module_XLabs_Platform_info;
extern void *mono_aot_module_XLabs_Core_info;
extern void *mono_aot_module_System_Text_Encoding_info;
extern void *mono_aot_module_ExifLib_info;
extern void *mono_aot_module_Xamarin_Forms_Xaml_info;
extern void *mono_aot_module_System_Text_RegularExpressions_info;
extern void *mono_aot_module_XLabs_Ioc_info;
extern void *mono_aot_module_Plugin_Geolocator_Abstractions_info;
extern void *mono_aot_module_System_Net_Requests_info;
extern void *mono_aot_module_Plugin_Geolocator_info;
extern void *mono_aot_module_Lotz_Xam_Messaging_Abstractions_info;
extern void *mono_aot_module_Lotz_Xam_Messaging_info;
extern void *mono_aot_module_Newtonsoft_Json_info;
extern void *mono_aot_module_System_Xml_XDocument_info;
extern void *mono_aot_module_System_Xml_Linq_info;
extern void *mono_aot_module_System_Runtime_Serialization_Primitives_info;
extern void *mono_aot_module_System_Text_Encoding_Extensions_info;
extern void *mono_aot_module_System_Diagnostics_Tools_info;
extern void *mono_aot_module_Microsoft_CSharp_info;
extern void *mono_aot_module_Mono_CSharp_info;
extern void *mono_aot_module_Refractored_Xam_Settings_info;
extern void *mono_aot_module_Refractored_Xam_Settings_Abstractions_info;
extern void *mono_aot_module_XLabs_Forms_iOS_info;
extern void *mono_aot_module_XLabs_Forms_info;
extern void *mono_aot_module_XLabs_Serialization_info;
extern void *mono_aot_module_System_Runtime_Serialization_Xml_info;
extern void *mono_aot_module_XLabs_Platform_iOS_info;

void xamarin_register_modules_impl ()
{
	mono_aot_register_module (mono_aot_module_TCDButtonCrossPlatformiOS_info);
	mono_aot_register_module (mono_aot_module_mscorlib_info);
	mono_aot_register_module (mono_aot_module_System_Core_info);
	mono_aot_register_module (mono_aot_module_System_info);
	mono_aot_register_module (mono_aot_module_System_Xml_info);
	mono_aot_register_module (mono_aot_module_Mono_Dynamic_Interpreter_info);
	mono_aot_register_module (mono_aot_module_Xamarin_iOS_info);
	mono_aot_register_module (mono_aot_module_Xamarin_Forms_Maps_iOS_info);
	mono_aot_register_module (mono_aot_module_Xamarin_Forms_Platform_iOS_info);
	mono_aot_register_module (mono_aot_module_Xamarin_Forms_Core_info);
	mono_aot_register_module (mono_aot_module_System_Runtime_info);
	mono_aot_register_module (mono_aot_module_System_ComponentModel_Composition_info);
	mono_aot_register_module (mono_aot_module_System_ObjectModel_info);
	mono_aot_register_module (mono_aot_module_System_IO_info);
	mono_aot_register_module (mono_aot_module_System_Collections_info);
	mono_aot_register_module (mono_aot_module_System_Threading_Tasks_info);
	mono_aot_register_module (mono_aot_module_System_Globalization_info);
	mono_aot_register_module (mono_aot_module_System_ComponentModel_info);
	mono_aot_register_module (mono_aot_module_System_Linq_Expressions_info);
	mono_aot_register_module (mono_aot_module_System_Xml_ReaderWriter_info);
	mono_aot_register_module (mono_aot_module_System_Reflection_info);
	mono_aot_register_module (mono_aot_module_System_Dynamic_Runtime_info);
	mono_aot_register_module (mono_aot_module_System_Threading_info);
	mono_aot_register_module (mono_aot_module_System_Runtime_Extensions_info);
	mono_aot_register_module (mono_aot_module_System_Linq_info);
	mono_aot_register_module (mono_aot_module_System_Reflection_Extensions_info);
	mono_aot_register_module (mono_aot_module_System_Diagnostics_Debug_info);
	mono_aot_register_module (mono_aot_module_Xamarin_Forms_Platform_info);
	mono_aot_register_module (mono_aot_module_System_Net_Http_info);
	mono_aot_register_module (mono_aot_module_System_Runtime_Serialization_info);
	mono_aot_register_module (mono_aot_module_System_ServiceModel_Internals_info);
	mono_aot_register_module (mono_aot_module_Xamarin_Forms_Maps_info);
	mono_aot_register_module (mono_aot_module_System_Resources_ResourceManager_info);
	mono_aot_register_module (mono_aot_module_TCDButtonCrossPlatform_info);
	mono_aot_register_module (mono_aot_module_XLabs_Platform_info);
	mono_aot_register_module (mono_aot_module_XLabs_Core_info);
	mono_aot_register_module (mono_aot_module_System_Text_Encoding_info);
	mono_aot_register_module (mono_aot_module_ExifLib_info);
	mono_aot_register_module (mono_aot_module_Xamarin_Forms_Xaml_info);
	mono_aot_register_module (mono_aot_module_System_Text_RegularExpressions_info);
	mono_aot_register_module (mono_aot_module_XLabs_Ioc_info);
	mono_aot_register_module (mono_aot_module_Plugin_Geolocator_Abstractions_info);
	mono_aot_register_module (mono_aot_module_System_Net_Requests_info);
	mono_aot_register_module (mono_aot_module_Plugin_Geolocator_info);
	mono_aot_register_module (mono_aot_module_Lotz_Xam_Messaging_Abstractions_info);
	mono_aot_register_module (mono_aot_module_Lotz_Xam_Messaging_info);
	mono_aot_register_module (mono_aot_module_Newtonsoft_Json_info);
	mono_aot_register_module (mono_aot_module_System_Xml_XDocument_info);
	mono_aot_register_module (mono_aot_module_System_Xml_Linq_info);
	mono_aot_register_module (mono_aot_module_System_Runtime_Serialization_Primitives_info);
	mono_aot_register_module (mono_aot_module_System_Text_Encoding_Extensions_info);
	mono_aot_register_module (mono_aot_module_System_Diagnostics_Tools_info);
	mono_aot_register_module (mono_aot_module_Microsoft_CSharp_info);
	mono_aot_register_module (mono_aot_module_Mono_CSharp_info);
	mono_aot_register_module (mono_aot_module_Refractored_Xam_Settings_info);
	mono_aot_register_module (mono_aot_module_Refractored_Xam_Settings_Abstractions_info);
	mono_aot_register_module (mono_aot_module_XLabs_Forms_iOS_info);
	mono_aot_register_module (mono_aot_module_XLabs_Forms_info);
	mono_aot_register_module (mono_aot_module_XLabs_Serialization_info);
	mono_aot_register_module (mono_aot_module_System_Runtime_Serialization_Xml_info);
	mono_aot_register_module (mono_aot_module_XLabs_Platform_iOS_info);

}

void xamarin_register_assemblies_impl ()
{
	guint32 exception_gchandle = 0;
	xamarin_open_and_register ("Xamarin.iOS.dll", &exception_gchandle);
	xamarin_process_managed_exception_gchandle (exception_gchandle);
	xamarin_open_and_register ("Xamarin.Forms.Maps.iOS.dll", &exception_gchandle);
	xamarin_process_managed_exception_gchandle (exception_gchandle);
	xamarin_open_and_register ("Xamarin.Forms.Platform.iOS.dll", &exception_gchandle);
	xamarin_process_managed_exception_gchandle (exception_gchandle);
	xamarin_open_and_register ("Plugin.Geolocator.dll", &exception_gchandle);
	xamarin_process_managed_exception_gchandle (exception_gchandle);
	xamarin_open_and_register ("XLabs.Forms.iOS.dll", &exception_gchandle);
	xamarin_process_managed_exception_gchandle (exception_gchandle);
	xamarin_open_and_register ("XLabs.Platform.iOS.dll", &exception_gchandle);
	xamarin_process_managed_exception_gchandle (exception_gchandle);

}

void xamarin_create_classes();
void xamarin_setup_impl ()
{
	xamarin_use_old_dynamic_registrar = FALSE;
	xamarin_create_classes();
	xamarin_gc_pump = FALSE;
	xamarin_init_mono_debug = TRUE;
	xamarin_compact_seq_points = FALSE;
	xamarin_executable_name = "TCDButtonCrossPlatformiOS.exe";
	xamarin_use_new_assemblies = 1;
	mono_use_llvm = FALSE;
	xamarin_log_level = 2;
	xamarin_arch_name = "armv7";
	xamarin_marshal_managed_exception_mode = MarshalManagedExceptionModeDisable;
	xamarin_marshal_objectivec_exception_mode = MarshalObjectiveCExceptionModeDisable;
	xamarin_debug_mode = TRUE;
	setenv ("MONO_GC_PARAMS", "nursery-size=512k", 1);
}

int main (int argc, char **argv)
{
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
	int rv = xamarin_main (argc, argv, false);
	[pool drain];
	return rv;
}
void xamarin_initialize_callbacks () __attribute__ ((constructor));
void xamarin_initialize_callbacks ()
{
	xamarin_setup = xamarin_setup_impl;
	xamarin_register_assemblies = xamarin_register_assemblies_impl;
	xamarin_register_modules = xamarin_register_modules_impl;
}
